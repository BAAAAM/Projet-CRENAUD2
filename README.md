# Project : CRENAUD2 - Causing A Random Election : a New Alternative Universe is the Destination


Have you ever wondered about how the universe was strange ? Well, you were
right. In fact, only a few of theses weird things the universe is capable of ever
reach our hears. It happens to exist an alternate reality, in parallel of our world.
This reality, due to the the curious randomness of laws we don’t understand, is
strictly identical to our own, supposing we forget about a tiny little detail : in
this alternate universe, Nazis won the World War Two. But it’s not the story
we are gonna tell you today.

As our hero lands in the US after a difficult flight, ready for his overseas semester,
he is welcomed by a suspicious merchant. Managing to escape brilliantly, he
then encounter the mighty Bernie Sanders, offering him to join his forces in the
US presidential campaign. After a difficult start, Bernie decides to change his
plans. But this reversal doesn’t seem natural, and evil forces are at work. . . Will
our hero be able to see through the fog of mystery shrouding this campaign ?
Don’t expect that much from him, he’s a bit stupid after all.



## A Lemming Game, but an incredible one !

Although CRENAUD2 is a clone of the famous *Lemming* game, it's not exactly a Lemming game. 
The second main opus of the incredible *CRENAUD* saga is the game you were waiting for all of your life. 
* Humor ? check. 
* Awesomeness ? check.
* Power of friendship ? check.
* Non-sense ? check, check, and check, and check. check-check-check, check, check, and again, and check, twice, check.



## Strong story-telling

CRENAUD2 features a strong story-telling feature, using the proven japanese visual novel system to create more-than-real characters and develop an awesome story.


## Extensibility in mind

The whole game is loaded from JSON files, which means you can create your own incredible story by yourself !
You can also create your own levels by using Tiled, an external (free) software aiming at creating map. Yay ! We really rock.


## The team

* Antoine Bartuccio : Also known as Bépo-man, although this layout is useless (still better than AZERTY anyway).
* Magsen Abbe : Also called Bullshit-man. Of course, bullshit wasn't needed for this project.
* Arthur Amalvy : Also known as regex-man, even if no regex were needed here. Booh.
* Vincent Chapuis : Also called I-have-a-question-about-the-physical-engine-man.


## Trivia

* This project is ISO-1664 certified. We're very proud of that.
* Magsen's name starts with an "M".
* Do You Love Strange Teddy Bear Under The Blue Moon ? We don't. They're scary as hell.
* Beware of the Monster.
* No time for fooling around !
* It's not nice to be mean.
* The occulus rift compatibility has been released. It’s a secret option, may the force be with you to find it. 

![ISO 1664](iso1664.jpg)


## Emacs vs Vim

We do not thrive to give an answer to this question, as it may be too controversial. However, I'll give you an hint :

*it's obvious which one is better*




