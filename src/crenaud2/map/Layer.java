package crenaud2.map;


import crenaud2.physics.Hitbox;
import crenaud2.physics.Vector;
import javafx.scene.image.Image;

import java.util.ArrayList;

/**
 * Created by aethor on 05/05/17.
 */
public class Layer{

    private ArrayList<Block> blocks;
    private ArrayList<Image> imageSet;
    private int stage;
    private int width;
    private int tileWidth;
    private int tileHeight;
    private int height;
    private String name;

    /**
     * Standard constructor for a layer
     * @param stage a positive integer representing the deep of the layer, 0 being the higher.
     * @param data the data in csv format, used to genereate blocks
     * @param imageSet the set of possible image for the layer
     * @param width the width of the layer in tile
     * @param height the height of the layer in tile
     */
    public Layer(int stage, String data, ArrayList<Image> imageSet, int width, int height, String name){

        this.stage = stage;
        this.imageSet = imageSet;
        this.width = width;
        this.height = height;
        this.name = name;

        loadBlocks(data);

    }

    /**
     * load all needed Blocks in the layer. load Physical blocks for the first layer, normal blocks for the other. Beware that tiled inverts layer order in tilemap's files
     * @param data the data in CSV format from the file
     */
    private void loadBlocks(String data){

        this.blocks = new ArrayList<Block>();
        String[] tilesID = data.split(",");

        this.tileWidth = (int) imageSet.get(0).getWidth();
        this.tileHeight = (int) imageSet.get(0).getHeight();
        Vector pos = new Vector(tileWidth/2, tileHeight/2);

        if(stage == 0){

            for(String tileID : tilesID){

                if(Integer.parseInt(tileID) != 0){//avoid error when block is null

                    Block block = new Block(imageSet.get(Integer.parseInt(tileID) - 1 ), new Vector(pos.getX(), pos.getY()));
                    block.hasPhysicalBody = false;
                    blocks.add(block);

                }

                if(pos.getX() <= this.width * tileWidth - tileWidth){
                    pos.setX(pos.getX() + tileWidth);
                }
                else{
                    pos.setX(tileWidth/2);
                    pos.setY(pos.getY() + tileHeight);
                }


            }

        }
        else{

            for(String tileID : tilesID){

                if(Integer.parseInt(tileID) != 0){//avoid error when block is null

                    Hitbox hitbox = new Hitbox(pos.getX() - tileWidth/2, pos.getY() - tileHeight/2, pos.getX() + tileWidth/2, pos.getY() + tileHeight/2);
                    Block block = new Block(imageSet.get(Integer.parseInt(tileID) - 1), hitbox, new Vector(pos.getX(), pos.getY()));
                    block.hasPhysicalBody = true;
                    if(this.name.equals("end")){
                        block.addProperty("end");
                    }
                    blocks.add(block);

                }

                if(pos.getX() <= this.width * tileWidth - tileWidth){
                    pos.setX(pos.getX() + tileWidth);
                }
                else{
                    pos.setX(tileWidth/2);
                    pos.setY(pos.getY() + tileHeight);
                }

            }

        }


    }

    /**
     * gives the width of this layer
     * @return the width of this layer, in tiles
     */
    public int getWidth() {
        return width;
    }

    /**
     * gives the height of this layer
     * @return the height of this layer, in tiles
     */
    public int getHeight() {
        return height;
    }

    /**
     * gives the width of a tile in this layer
     * @return the width of a tile, in pixel
     */
    public int getTileWidth() {
        return tileWidth;
    }

    /**
     * gives the height of a tile in this layer
     * @return the height of a tile, in pixel
     */
    public int getTileHeight() {
        return tileHeight;
    }

    /**
     * gives the stage of this layer ( 0 corresponding to the first layer, 1 to the second, etc... )
     * @return the stage of this layer
     */
    public int getStage() {
        return stage;
    }

    /**
     * gives the list of block in this layer, in horizontal order
     * @return a ArrayList of Block
     */
    public ArrayList<Block> getBlocks() {
        return blocks;
    }

    /**
     * gives the list of possible images for this layer
     * @return an ArrayList of Image for use with javaFX
     */
    public ArrayList<Image> getImageSet() {
        return imageSet;
    }

}
