package crenaud2.map;

import crenaud2.physics.Hitbox;
import crenaud2.physics.Vector;
import javafx.scene.image.Image;

/**
 * Created by klmp200 on 23/06/2017.
 */
public class Stair extends Block {
    /**
     * Standard constructor for a non physical block
     *  @param image    the JavaFX Image the block will have to display
     * @param position the position Vector of the block
     */
    public Stair(Image image, Vector position) {
        super(image, position);
        hasPhysicalBody = true;
    }

    /**
     * Constructor for a physical block
     *
     * @param image    the Image of this block
     * @param hitbox   the hitbox for this block
     * @param position the position of the block ( remember it cannot move )
     */
    public Stair(Image image, Hitbox hitbox, Vector position) {
        super(image, hitbox, position);
    }

    public String toString(){
        return super.toString();
    }

}
