package crenaud2.map;

import crenaud2.physics.Hitbox;
import crenaud2.physics.PhysicalObject;
import crenaud2.physics.Vector;
import javafx.scene.image.Image;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by aethor on 05/05/17.
 */

/**
 * block which has physical attributes ( handle collision, etc... )
 */
public class Block extends PhysicalObject{

    public boolean hasPhysicalBody;
    private Image image;
    private Vector position;
    private ArrayList<String> properties;

    /**
     * Standard constructor for a non physical block
     * @param image the JavaFX Image the block will have to display
     * @param position the position Vector of the block
     */
    public Block(Image image, Vector position){

        super(position);
        this.hitbox = null;
        this.image = image;
        this.position = position;
        this.hasPhysicalBody = false;
        this.properties = new ArrayList<>();

    }


    /**
     * Constructor for a physical block
     * @param image the Image of this block
     * @param hitbox the hitbox for this block
     * @param position the position of the block ( remember it cannot move )
     */
    public Block(Image image, Hitbox hitbox, Vector position){

        super(position, hitbox);
        this.hasPhysicalBody = true;
        this.image = image;
        this.position = position;
        this.setHitbox(hitbox);
        this.properties = new ArrayList<>();

    }


    public String toString(){
        return position.toString() + " - hasPhysicalBody : " + hasPhysicalBody;
    }


    /**
     * return the image used by the block
     * @return a JavaFX Image
     */
    public Image getImage() {
        return image;
    }

    /**
     * set the image of a block
     * @param image the JavaFX image which will replace the current image
     */
    public void setImage(Image image) {
        this.image = image;
    }

    /**
     * set the hitbox of a block
     * @param hitbox the Hitbox object which will replace the current image
     */
    public void setHitbox(Hitbox hitbox){
        if(this.hasPhysicalBody == true){
            this.hitbox = hitbox;
        }
        else{
            throw new NullPointerException("block has no physical body");
        }
    }

    /**
     * set position of the block. If the block has a physical body, it'll set his position too.
     * @param position
     */
    public void setPosition(Vector position){
        if(this.hasPhysicalBody == true){
            this.setPosition(position);
        }
        this.position = position;
    }

    /**
     * gives the position vector of the block
     * @return A crenaud2.Vector object containing the position of the block
     */
    public Vector getPosition() {
        return position;
    }

    public void addProperty(String property){
        this.properties.add(property);
    }

    public boolean hasProperty(String property){
        return this.properties.contains(property);
    }


}
