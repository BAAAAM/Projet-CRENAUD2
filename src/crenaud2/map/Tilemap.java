package crenaud2.map;

import crenaud2.lemmings.Lemming;
import im.bci.tmxloader.*;
import javafx.scene.image.Image;

import javafx.scene.image.PixelReader;
import javafx.scene.image.WritableImage;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Scanner;


/**
 * Created by aethor on 03/05/17.
 */
public class Tilemap {

    private ArrayList<Layer> layers;
    private int width;
    private int height;


    /**
     * Standard constructor for a tilemap
     * @param path the path to the tilemap file
     * @throws IOException
     */
    public Tilemap(String path) throws IOException {

        this.width = 0;
        this.height = 0;
        layers = this.loadLevel(path);

    }


    /**
     * return a TmxMap object after parsing the tilemap file
     * @param path the path to the tilemap file
     * @return a TmxMap Object
     * @throws IOException
     */
    private TmxMap loadMap(String path) /*throws IOException*/ {

        TmxLoader loader = new TmxLoader();
        TmxMap map = new TmxMap();

        try {
            loader.parseTmx(map, readFile(path));
        } catch (IOException e) {
            e.printStackTrace();
        }

        for(TmxTileset tileset : map.getTilesets()){

            tileset.getImage().setSource(tileset.getImage().getSource().substring(2, tileset.getImage().getSource().length()));

        }

        loader.decode(map);


        return map;

    }

    /**
     * Create every layer and every block in every layer of the map
     * @param path the path to the map's file in tmx format
     * @return the list of layers of the map
     * @throws IOException
     */
    public ArrayList<Layer> loadLevel(String path) throws IOException {

        TmxMap map = loadMap(path);
        ArrayList<Layer> list = new ArrayList<Layer>();
        ArrayList<Image> imageSet = loadImageSet(map);
        int i = 0;

        for(TmxLayer layer : map.getLayers()){

            list.add(new Layer(i, layer.getData().getData(), imageSet, layer.getWidth(), layer.getHeight(), layer.getName()));

            this.width = Math.max(width, layer.getWidth());
            this.height = Math.max(height, layer.getHeight());

            i++;

        }

        return list;

    }

    /**
     * read the whole content of a file
     * @param path the path to the file which will be read
     * @return the content of the file
     * @throws IOException
     */
    private String readFile(String path) throws IOException{

        Scanner reader = new Scanner(getClass().getResourceAsStream(path)).useDelimiter("\\Z");//\Z is the end of the file
        return reader.next();

    }


    /**
     * This function cut images from tilemaps to enable easy access
     * @param map the TmxMap of this map
     * @return the list of all possible imageView for a block in this map
     */
    private ArrayList<Image> loadImageSet(TmxMap map){

        ArrayList<Image> list = new ArrayList<Image>();

        for(TmxTileset tileset : map.getTilesets()){

            TmxImage image = tileset.getImage();
            int viewNumber = image.getWidth()/tileset.getTilewidth() * image.getHeight()/tileset.getTileheight();
            int widthInTile = image.getWidth()/tileset.getTilewidth();
            int heightInTile = image.getHeight()/tileset.getTileheight();

            for(int i = 0; i < heightInTile; i++){

                for(int j = 0; j < widthInTile; j++){

                    PixelReader pixelReader = new Image(image.getSource()).getPixelReader();
                    list.add(new WritableImage(pixelReader, j * tileset.getTilewidth(), i * tileset.getTileheight(), tileset.getTilewidth(), tileset.getTileheight()));

                }

            }



        }

        return list;

    }

    /**
     * return this list of layers in this level
     * @return the list of layers of this level as an ArrayList of Layer
     */
    public ArrayList<Layer> getLayers() {
        return layers;
    }

    /**
     * return the width of the level ( which is the max width of one of it's layer )
     * @return the height of the level in tile
     */
    public int getWidth() {
        return width;
    }

    /**
     * return the height of the level ( which is the max height of one of it's layer )
     * @return the height of the level in tile
     */
    public int getHeight() {
        return height;
    }

}
