package crenaud2;
/**
 * Created by klmp200 on 02/05/2017.
 */

import crenaud2.gamecontroller.GameController;
import crenaud2.gamecontroller.Stage;
import crenaud2.gameplay.Level;
import crenaud2.gameplay.LevelBuilder;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.scene.Parent;
import javafx.scene.Scene;

import java.io.IOException;
import java.net.URL;

/**
 * Main app of the CRENAUD2 levels
 */
public class Crenaud2 extends Application {

    private javafx.stage.Stage stage;
    private GameController game;
    private Stage currentLevel;
    private FXMLLoader currentLoader;
    private Scene currentScene;
    private static boolean debug;

    public static void main(String[] args) {

        if(args.length > 0 && args[0].equals("debug")){ //Todo : maybe find another way, but it'll be fine for now è.é \Aethor
            debug = true;
        }
        else{
            debug = false;
        }

        launch(args);
    }

    /**
     * Start a new app
     * @param primaryStage as Stage
     * @throws IOException if error occurs reading a json
     */
    @Override
    public void start(javafx.stage.Stage primaryStage) throws IOException {
        Stage current = null;
        this.game = new GameController("/json/controller.godwin");
        this.stage = primaryStage;

        this.nextLevel();
    }

    /**
     * Launch the next map
     * @throws IOException if error occurs reading a file
     */
    public void nextLevel(boolean win) throws IOException {
        if(win){
            if (this.game.hasNextStage()){
                this.currentLevel = this.game.getNextStage();

            }
            else{
                Platform.exit();
            }
        }

        this.loadLevel();
        stage.show();

    }

    public void nextLevel() throws IOException {
        nextLevel(true);
    }

    /**
     * Load a next level by save method
     * @param code String
     */
    public void nextLevel(String code) throws IOException {
        this.currentLevel = game.getStageByCode(code);
        this.loadLevel();
        stage.show();
    }

    /**
     * Load a new map given a map descriptor
     * @throws IOException if error occurs while reading a file
     */
    private void loadLevel() throws IOException {
        if (this.currentLevel.getType().equals("cinematic")){
            this.loadFXML("fxml/dialogues.fxml", "css/cinematics.css");
            this.stage.setTitle("Cinematic");
        } else if (this.currentLevel.getType().equals("level")){
            this.loadFXML("fxml/game.fxml", "css/game.css");
            this.stage.setTitle("Actual Gameplay");
        } else if (this.currentLevel.getType().equals("menu")){
            this.loadFXML("fxml/mainMenu.fxml", "css/mainMenu.css");
        }

        /* launch the window */
        ((FXMLController) this.currentLoader.getController()).setApplication(this);
        ((FXMLController) this.currentLoader.getController()).setCode(currentLevel.getCode());
        ((FXMLController) this.currentLoader.getController()).loadJson(currentLevel.getFilepath());
    }

    /**
     * Create a new FXMLLoader and give it a fxml and a css
     * @param fxml as String
     * @param css as String
     * @throws IOException for error while handeling files
     */
    private void loadFXML(String fxml, String css) throws IOException {
        this.loadFXML(fxml);
        this.currentScene.getStylesheets().add(getClass().getClassLoader().getResource(css).toExternalForm());

    }

    /**
     * Create a new FXMLLoader and give it a fxml
     * @param fxml as String
     * @throws IOException for error while handeling files
     */
    private void loadFXML(String fxml) throws IOException {
        URL location = getClass().getClassLoader().getResource(fxml);
        this.closeWindow();
        this.currentLoader = new FXMLLoader();
        this.currentLoader.setLocation(location);
        this.currentLoader.setBuilderFactory(new JavaFXBuilderFactory());
        assert location != null;
        Parent root = this.currentLoader.load(location.openStream());
        this.currentScene = new Scene(root);
        this.stage.setScene(this.currentScene);
    }

    /**
     * Set loader and scene to null and stop everything
     */
    private void closeWindow() {
        if (this.currentLoader != null){
            ((FXMLController) this.currentLoader.getController()).clean();
        }
        this.currentLoader = null;
        this.currentScene = null;
    }

    /**
     * Know if a given code exists in this game
     * @param code a String
     * @return boolean
     */
    protected boolean hasGivenLevelCode(String code){
        Stage s = game.getStageByCode(code);
        game.resetStages();
        game.skipStage();
        return s != null;
    }

    /**
     * This is a dirty trick that will help us after
     * @return boolean
     */
    public static boolean isDebug(){
        return debug;
    }

    @Override public void stop(){
        ((FXMLController) this.currentLoader.getController()).clean();
    }
}
