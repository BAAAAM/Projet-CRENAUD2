package crenaud2;

/**
 * Created by klmp200 on 09/05/2017.
 */

import javafx.fxml.FXML;
import javafx.scene.control.Label;

import java.io.IOException;

/**
 * Interface to manage FXMLControllers
 */
public interface FXMLController {

    /**
     * Should load a json from a given path and launch the game
     * @param json as String
     */
    public void loadJson(String json) throws IOException;

    /**
     * Unload everything (like music)
     */
    public void clean();

    /**
     * Set a reference to the main programme
     * @param app Crenaud2
     */
    public void setApplication(Crenaud2 app);

    /**
     * Set the code to display to reach this level
     * @param code
     */
    public void setCode(String code);

}
