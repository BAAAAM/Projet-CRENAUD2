package crenaud2.cinematics;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import javafx.scene.image.Image;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by klmp200 on 30/04/2017.
 */

/**
 * A character class
 * Create a new character from a given json path
 */
public class Character {
    private InputStream filePath;
    private String firstName;
    private String lastName;
    private boolean isARobot;
    private Map<String, Sprite> sprites = new HashMap<>();

    /**
     * Creates a new character
     * @param path of the json file
     * @throws IOException if error occurs while reading the file
     */
    public Character(String path) throws IOException {
        this.filePath = getClass().getResourceAsStream(path);
        this.parse();
    }

    /**
     * Function that read the json and generate the character
     * @throws IOException if error occurs while reading the file
     */
    private void parse() throws IOException {
        Gson g = new Gson();
        JsonReader reader = g.newJsonReader(new InputStreamReader(filePath));
        String name = null;

        reader.beginObject();
        while (reader.hasNext()){
            name = reader.nextName();
            if (name.equals("first name"))
                this.firstName = reader.nextString();
            else if (name.equals("last name"))
                this.lastName = reader.nextString();
            else if (name.equals("sprites"))
                this.parseSprites(reader);
            else if (name.equals("is a robot"))
                this.isARobot = reader.nextBoolean();
            else
                reader.skipValue();
        }
    }

    /**
     * Funny function that return if the character is a robot
     * @return boolean
     */
    public boolean isARobot() {
        return isARobot;
    }

    /**
     * Get first name of the character
     * @return first name as String
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Get last name of the character
     * @return last name as String
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Get sprite according to a given behavior
     * @param behavior as String
     * @return Image from Sprite class
     */
    public Image getSprite(String behavior){
        Sprite s = sprites.get(behavior);
        if (s != null)
            return s.getImg();
        else
            return null;
    }

    /**
     * Generate sprite map according to json
     * @param reader as JsonReader
     * @throws IOException if error occurs while reading the file
     */
    private void parseSprites(JsonReader reader) throws IOException {
        String behavior = null;
        Sprite s = null;

        reader.beginObject();
        while (reader.hasNext()){
            behavior = reader.nextName();
            try {
                s = new Sprite(reader.nextString());
                this.sprites.put(behavior, s);
            } catch (FileNotFoundException e){}
        }
    }

    @Override
    public String toString() {
        return "Character{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", filePath='" + filePath + '\'' +
                ", isARobot='" + isARobot + '\'' +
                ", sprites=" + sprites.keySet().toString() +
                '}';
    }
}
