package crenaud2.cinematics;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import javafx.scene.image.Image;
import javafx.scene.media.Media;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by klmp200 on 30/04/2017.
 */

/**
 * Contains information from an automated build scene
 */
public class SceneBuilder {

    private List<Image> backgrounds = new ArrayList<>();
    private List<Media> musics = new ArrayList<>();
    private List<Character> characters = new ArrayList<>();
    private List<Dialogue> dialogues = new ArrayList<>();

    /**
     * Build a new SceneBuilder from a json path
     * @param path as a string
     * @throws IOException if error occurs while reading the file
     */
    public SceneBuilder(String path) throws IOException {
        Gson g = new Gson();
        JsonReader reader = g.newJsonReader(new InputStreamReader(getClass().getResourceAsStream(path)));
        String name = null;

        reader.beginObject();
        while(reader.hasNext()){
            name = reader.nextName();
            if (name.equals("characters"))
                this.parseCharacters(reader);
            else if (name.equals("backgrounds"))
                this.parseBackgrounds(reader);
            else if (name.equals("dialogues"))
                this.parseDialogues(reader);
            else if (name.equals("musics"))
                this.parseMusics(reader);
            else
                reader.skipValue();
        }
        reader.endObject();
    }

    /**
     * Create a list of character from json array
     * @param reader as JsonReader
     * @throws IOException if error occurs while reading the file
     */
    private void parseCharacters(JsonReader reader) throws IOException {
        reader.beginArray();
        while(reader.hasNext()){
            this.characters.add(new Character(reader.nextString()));
        }
        reader.endArray();
    }

    /**
     * Create a list of backgrounds from json array
     * @param reader as JsonReader
     * @throws IOException if error occurs while reading the file
     */
    private void parseBackgrounds(JsonReader reader) throws IOException {
        reader.beginArray();
        Image img = null;
        while(reader.hasNext()){
            try {
                img = new Image(getClass().getResourceAsStream(reader.nextString()));
            } catch (Exception e){
                img = null;
            }
            this.backgrounds.add(img);
        }
        reader.endArray();
    }

    /**
     * Create a list of Dialogue from json array
     * @param reader as JsonReader
     * @throws IOException if error occurs while reading the file
     */
    private void parseDialogues(JsonReader reader) throws IOException {
        reader.beginArray();
        while(reader.hasNext()){
            this.dialogues.add(new Dialogue(reader));
        }
        reader.endArray();
    }

    /**
     * Get the list of backgrounds
     * @return list of Image
     */
    public List<Image> getBackgrounds() {
        return backgrounds;
    }

    /**
     * Get the list of musics
     * @return list of Media
     */
    public List<Media> getMusics() {
        return musics;
    }

    /**
     * Get the list of characters
     * @return list of Character
     */
    public List<Character> getCharacters() {
        return characters;
    }

    /**
     * Get the list of dialogues
     * @return list of Dialogue
     */
    public List<Dialogue> getDialogues() {
        return dialogues;
    }

    /**
     * Create a list of music from json array
     * @param reader as JsonReader
     * @throws IOException if error occurs while reading the file
     */
    private void parseMusics(JsonReader reader) throws IOException {
        Media music = null;
        reader.beginArray();
        while(reader.hasNext()){
            try {
                music = new Media(getClass().getClassLoader().getResource(reader.nextString()).toExternalForm());
            } catch (Exception e){
                e.printStackTrace();
                music = null;
            }
            this.musics.add(music);
        }
        reader.endArray();
    }
}
