package crenaud2.cinematics;

import javafx.scene.image.Image;
import javafx.scene.media.Media;

import java.io.IOException;

/**
 * Created by klmp200 on 30/04/2017.
 */

/**
 * A Scene object
 * Encapsulate a SceneBuilder and give it easy access for user
 */
public class Scene {

    private SceneBuilder descriptor;
    private Integer dialogueNb = 0;

    /**
     * Create a new Scene from a given json path
     * @param path as String
     */
    public Scene(String path) {
        try {
            this.descriptor = new SceneBuilder(path);
        } catch (IOException e) {
            this.descriptor = null;
        }
    }

    /**
     * Check if another dialogue is available
     * @return boolean
     */
    public boolean hasNextDialogue(){
        return dialogueNb < this.numberDialogues();
    }

    /**
     * Return number of dialogues in the scene
     * @return integer
     */
    public int numberDialogues() {
        return this.descriptor.getDialogues().size();
    }

    /**
     * Skip a dialogue
     */
    public void skipDialogue(){
        this.dialogueNb++;
    }

    /**
     * Start dialogues from zero
     */
    public void resetDialogues(){
        this.dialogueNb = 0;
    }

    /**
     * Get next available dialogue
     * @return Dialogue
     */
    public Dialogue getNextDialogue(){
        Dialogue d = this.descriptor.getDialogues().get(this.dialogueNb);
        this.dialogueNb++;
        return d;
    }

    /**
     * Get a precise dialogue from the scene
     * @param id as integer
     * @return Dialogue
     */
    public Dialogue getDialogue(int id){
        if (id >= 0 && id < this.descriptor.getCharacters().size())
            return this.descriptor.getDialogues().get(id);
        else
            return null;
    }

    /**
     * Get a music from the music list of the scene
     * @param id as integer
     * @return Media
     */
    public Media getMusic(int id){
        if (id >= 0 && id < this.descriptor.getCharacters().size())
            return this.descriptor.getMusics().get(id);
        else
            return null;
    }

    /**
     * Get a character from the character list of the scene
     * @param id as integer
     * @return Character
     */
    public Character getCharacter(int id){
        if (id >= 0 && id < this.descriptor.getCharacters().size())
            return this.descriptor.getCharacters().get(id);
        else
            return null;
    }

    /**
     * Get a background from the background list of the scene
     * @param id as integer
     * @return Image
     */
    public Image getBackground(int id){
        if (id >= 0 && id < this.descriptor.getBackgrounds().size())
            return this.descriptor.getBackgrounds().get(id);
        else
            return null;
    }
}
