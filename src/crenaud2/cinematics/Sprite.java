package crenaud2.cinematics;

import javafx.scene.image.Image;

import java.io.*;

/**
 * Created by klmp200 on 30/04/2017.
 */

/**
 * A simplistic representation of a sprite for use with javafx
 */
public class Sprite {
    private InputStream path;
    private Image img;


    /**
     * Get image of the sprite
     * @return Image of the sprite
     */
    public Image getImg() {
        return img;
    }

    /**
     * Generate a new sprite
     * @param path of the file
     * @throws FileNotFoundException if file not found
     */
    public Sprite(String path) throws FileNotFoundException {
        this.path = getClass().getResourceAsStream(path);
        this.img = new Image(this.path);
    }

    public Sprite(String path, int width, int height, boolean preserveRatio, boolean smooth) throws FileNotFoundException{
        this.path = getClass().getResourceAsStream(path);
        this.img = new Image(this.path, width, height, preserveRatio, smooth);
    }
}
