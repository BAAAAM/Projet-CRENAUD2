package crenaud2.cinematics;

import com.google.gson.stream.JsonReader;
import javafx.scene.media.Media;

import java.io.IOException;

/**
 * Created by klmp200 on 30/04/2017.
 */

/**
 * A dialogue class
 * Contains information about the talking character
 * Contains information about current background and music
 */
public class Dialogue {


    private int characterId;
    private int background = -1;
    private int music = -1;
    private String characterBehavior;
    private String characterPosition;
    private String text;
    private Media sound;
    private boolean isFinished;
    private boolean alone;

    /**
     * Create a new dialogue from a Json
     * @param reader a JsonReader
     * @throws IOException if error occurs while reading the file
     */
    public Dialogue(JsonReader reader) throws IOException {

        this.isFinished = false;
        this.alone = false;

        String name = null;
        reader.beginObject();
        while (reader.hasNext()){
            name = reader.nextName();
            if (name.equals("character"))
                this.parseCharacter(reader);
            else if (name.equals("text"))
                this.text = reader.nextString();
            else if (name.equals("sound")) {
                try {
                    this.sound = new Media(getClass().getClassLoader().getResource(reader.nextString()).toExternalForm());
                } catch (Exception e){
                    this.sound = null;
                }
            }
            else if (name.equals("background"))
                this.background = reader.nextInt();
            else if (name.equals("music"))
                this.music = reader.nextInt();
            else
                reader.skipValue();
        }
        reader.endObject();

    }

    /**
     * Add information about the talking character from a json
     * @param reader a JsonRedear
     * @throws IOException if error occurs while reading the file
     */
    private void parseCharacter(JsonReader reader) throws IOException {
        String name;
        reader.beginObject();

        while(reader.hasNext()){
            name = reader.nextName();
            if (name.equals("id"))
                this.characterId = reader.nextInt();
            else if (name.equals("behavior"))
                this.characterBehavior = reader.nextString();
            else if (name.equals("position"))
                this.characterPosition = reader.nextString();
            else if (name.equals("alone"))
                this.alone = reader.nextBoolean();
            else
                reader.skipValue();
        }
        reader.endObject();

    }

    /**
     * Get id of the current talking character
     * @return id of the character in the character list of the scene
     */
    public int getCharacterId() {
        return characterId;
    }


    /**
     * Get id of the current background
     * @return id of the backgrond in the background list of the scene
     */
    public int getBackground() {
        return background;
    }

    /**
     * Get id of the current music
     * @return id of the music in the music list of the scene
     */
    public int getMusic() {
        return music;
    }

    /**
     * Get the behavior of the current character
     * @return key for the map of behavior of a character
     */
    public String getCharacterBehavior() {
        return characterBehavior;
    }

    /**
     * Get position of the character on the screen
     * @return right or left
     */
    public String getCharacterPosition() {
        return characterPosition;
    }

    /**
     * Get the current dialogue text
     * @return text for the dialogue
     */
    public String getText() {
        return text;
    }

    /**
     * Get the sound emitted by the character
     * @return sound to play
     */
    public Media getSound() {
        return sound;
    }


    /**
     * Look if the display of the dialogue is finished
     * @return boolean
     */
    public boolean isFinished() {
        return isFinished;
    }

    /**
     * Mark the dialogue as finished
     * @param finished
     */
    public void setFinished(boolean finished) {
        isFinished = finished;
    }

    /**
     * Know if the character should be alone
     * @return boolean
     */
    public boolean isAlone() {
        return alone;
    }
}
