package crenaud2.cinematics;

import crenaud2.Crenaud2;
import crenaud2.FXMLController;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.image.ImageView;
import javafx.scene.media.MediaPlayer;
import javafx.util.Duration;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by klmp200 on 02/05/2017.
 */

/**
 * Control the display of a given json Scene
 */
public class FXMLSceneController implements FXMLController {

    private Scene scene;
    private MediaPlayer music;
    private MediaPlayer voice;
    private Crenaud2 app;
    private Timer dialogueDisplay;

    private Dialogue current;
    private final int dialogueSpeed = 50;

    private ColorAdjust unhighlight;

    @FXML private ImageView leftCharacter;
    @FXML private ImageView rightCharacter;
    @FXML private ImageView background;
    @FXML private Label nameDisplay;
    @FXML private Label textDisplay;
    @FXML private Label codeLabel;
    @FXML private Button skipButton;

    /**
     * Create a new Scene according to a given json path
     * @param json as string
     */
    public void loadJson(String json) {
        rightCharacter.setScaleX(-1);
        unhighlight = new ColorAdjust();
        unhighlight.setBrightness(-0.5);

        this.scene = new Scene(json);
        this.nextDialogue();
    }

    /**
     * Unload music
     */
    @Override
    public void clean() {
        if (music != null)
            music.stop();
        if (voice != null)
            voice.stop();
        if (app != null)
            app = null;
        if (dialogueDisplay != null) dialogueDisplay.cancel();
    }

    @Override
    public void setApplication(Crenaud2 app) {
        this.app = app;
    }

    /**
     * Set the code to display to reach this level
     *
     * @param code
     */
    @Override
    public void setCode(String code) {
        codeLabel.setText(code);
    }

    /**
     * Display correctly a character on the screen
     * @param current Dialogue
     * @param actor Charactor
     */
    private void displayCharacter(Dialogue current, Character actor){
        if (current.getCharacterPosition().equals("left")){
            leftCharacter.setImage(actor.getSprite(current.getCharacterBehavior()));
            leftCharacter.setEffect(null);
            if (current.isAlone())
                rightCharacter.setImage(null);
            else
                rightCharacter.setEffect(unhighlight);
        } else if (current.getCharacterPosition().equals("right")){
            rightCharacter.setImage(actor.getSprite(current.getCharacterBehavior()));
            rightCharacter.setEffect(null);
            if (current.isAlone())
                leftCharacter.setImage(null);
            else
                leftCharacter.setEffect(unhighlight);
        }
    }

    /**
     * Display next dialogue
     */
    private void nextDialogue() {
        if (scene.hasNextDialogue()){
            this.current = scene.getNextDialogue();
            Character actor = null;

            if (scene.getCharacter(current.getCharacterId()) != null) {

                actor = scene.getCharacter(current.getCharacterId());
                displayCharacter(current, actor);

                textDisplay.setText(String.valueOf(current.getText().charAt(0)));

                if (dialogueDisplay == null) dialogueDisplay = new Timer();
                else {
                    dialogueDisplay.cancel();
                    dialogueDisplay = new Timer();
                }

                dialogueDisplay.scheduleAtFixedRate(new TimerTask() {
                    @Override
                    public void run() {
                        Platform.runLater(new Runnable() {
                            @Override
                            public void run() {
                                textDisplay.setText(getNewDialogueString(dialogueDisplay));
                            }
                        });
                    }
                }, dialogueSpeed, dialogueSpeed);

                nameDisplay.setText(actor.getFirstName() + " " + actor.getLastName());

            }

            if (current.getBackground() >= 0 && scene.getBackground(current.getBackground()) != null)
                background.setImage(scene.getBackground(current.getBackground()));

            if (current.getMusic() >= 0 && scene.getMusic(current.getMusic()) != null){
                if (music != null)
                    music.stop();

                music = new MediaPlayer(scene.getMusic(current.getMusic()));
                music.setVolume(music.getVolume()/4);
                /* Auto repeat of music */
                music.setOnEndOfMedia(new Runnable() {
                    @Override
                    public void run() {
                        music.seek(Duration.ZERO);
                    }
                });
                music.play();
            }

            if (current.getSound() != null){
                if (voice != null)
                    voice.stop();

                voice = new MediaPlayer(current.getSound());
                voice.play();
            }

        } else {
            skipButton.setDisable(true);
            try {
                this.app.nextLevel();
            } catch (Exception e){
                /* todo quitter l'application */
                e.printStackTrace();
                System.out.println("Fin de l'application");
            }
        }
    }

    /**
     * Nice dialogue display
     * @param timer delay for display
     * @return String
     */
    private String getNewDialogueString(Timer timer){

        if(current.getText().equals(textDisplay.getText())){

            current.setFinished(true);
            timer.cancel();
            return current.getText();

        }
        else{

            return textDisplay.getText() + current.getText().charAt(textDisplay.getText().length());

        }

    }

    /**
     * Skip a single dialogue
     * @param event ActionEvent
     */
    @FXML
    public void handleSkipAction(ActionEvent event) {

        if(this.current.isFinished()){
            this.nextDialogue();
        }
        else{
            this.textDisplay.setText(current.getText());
            current.setFinished(true);
        }
    }

    /**
     * Skip all dialogues
     * @param event ActionEvent
     */
    @FXML
    public void handleSkipAllAction(ActionEvent event){
        while (this.scene.hasNextDialogue()) this.scene.skipDialogue();
        this.nextDialogue();
    }

}
