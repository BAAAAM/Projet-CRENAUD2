package crenaud2.lemmings;
import crenaud2.gameplay.Level;
import crenaud2.lemmings.skills.Skill;
import crenaud2.lemmings.skills.SkillBuilder;
import crenaud2.lemmings.skills.StairsBuilderSkill;
import crenaud2.map.Block;
import crenaud2.map.Stair;
import crenaud2.physics.*;
import javafx.scene.image.Image;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by aethor on 02/05/17.
 */
public class Lemming extends MovableObject{

    //Lemming image
    private Image image;
    private Hitbox footHitbox;
    private int horizontalDirection;
    private static final int horizontalSpeed = 25;
    private static final double mass = 50;
    private boolean grounded;
    private Animation walkAnimation;
    private Animation fallAnimation;
    private Animation currentAnimation;
    private Level currentLevel;
    private Skill skill;
    private List<String> properties = new ArrayList<>();

    public Lemming() throws FileNotFoundException {

        super();
        this.createImage();

    }

    public Lemming(double x, double y, Level level) throws FileNotFoundException {

        super(x, y, mass);

        horizontalDirection = 1;
        currentLevel = level;

        this.walkAnimation = currentLevel.getLemmingWalkAnimation().clone();
        this.fallAnimation = currentLevel.getLemmingFallAnimation().clone();
        this.createImage();

        this.setHitbox(new Hitbox(position.getX() - image.getWidth()/2 + 5,
                position.getY() - image.getHeight()/2,
                position.getX() + image.getWidth()/2 - 5,
                position.getY() + image.getHeight()/2 - 5));

        this.footHitbox = new Hitbox(position.getX() - image.getWidth()/2 + 5,
                position.getY() + image.getHeight()/2 - 5,
                position.getX() + image.getWidth()/2 - 5,
                position.getY() + image.getHeight()/2);


    }
    public Lemming(Vector position, Level level) throws FileNotFoundException {

        super(position, mass);

        horizontalDirection = 1;
        currentLevel = level;

        this.walkAnimation = currentLevel.getLemmingWalkAnimation().clone();
        this.fallAnimation = currentLevel.getLemmingFallAnimation().clone();
        this.createImage();

        this.setHitbox(new Hitbox(position.getX() - image.getWidth()/2 + 5,
                position.getY() - image.getHeight()/2,
                position.getX() + image.getWidth()/2 - 5,
                position.getY() + image.getHeight()/2 - 5));

        this.footHitbox = new Hitbox(position.getX() - image.getWidth()/2 + 5,
                position.getY() + image.getHeight()/2 - 5,
                position.getX() + image.getWidth()/2 - 5,
                position.getY() + image.getHeight()/2);


    }




    /**
     * Normal behavior of a lemming when colliding
     * @param other a PhysicalObject
     */
    public void collideNoSkill(PhysicalObject other){

        boolean collideWithFoot = this.footHitbox.isColliding(other.getHitbox());
        //boolean collideWithMain = this.hitbox.isColliding(other.getHitbox());//for optimisation purpose, I only calculate them once.

        this.grounded = true;

        if(other instanceof Block && ((Block) other).hasProperty("end")){//when lemming hit an "end" object, we consider that he's saved
            currentLevel.removeLemming(this, true);
        }

        if(collideWithFoot){

            if(speed.getY() > 60){//Todo : hardcoded #ISO1664
                currentLevel.removeLemming(this, false);
            }

            this.speed.setCoordinates(horizontalSpeed * horizontalDirection, 0);
            this.addUniqForce("antiGravity", this.forces.get("gravity").negate());

            if (other instanceof Stair && footHitbox.getYmax() > other.getHitbox().getCenter().getY()){

                double teleportX = position.getX() + 1 * horizontalDirection;
                double teleportY = position.getY() - other.getHitbox().getYmax() + other.getHitbox().getYmin();

                teleport(teleportX, teleportY);

                if(this.skill instanceof StairsBuilderSkill){
                    ((StairsBuilderSkill) this.skill).setUsedAtThisStage(false);
                }

            }

        }

        if(this.hitbox.isColliding(other.getHitbox()) && !(other instanceof Stair)){ // need to refresh the calculation in case of teleport
            if (getPosition().getX() < other.getPosition().getX()){
                horizontalDirection = -1;
            }
            else{
                horizontalDirection = 1;
            }
        }

        if(this.skill == null){
            currentAnimation = walkAnimation;
        }

    }

    /**
     * React when colliding
     * @param other a PhysicalObject
     */
    public void collide(PhysicalObject other){
        if (skill == null) collideNoSkill(other);
        else skill.collide(this, other);
    }

    /**
     * Normal behavior of a lemming when not colliding
     */
    public void notCollidedNoSkill(){

        if(isGrounded()){
            this.speed.setY(20);
        }
        grounded = false;

        forces.remove("antiGravity");
        speed.setX(0);

        if(this.skill == null){
            currentAnimation = fallAnimation;
        }

    }

    /**
     * React when not colliding
     */
    public void notCollided(){
        if (skill == null) notCollidedNoSkill();
        else skill.notCollided(this);
    }

    @Override
    public boolean isColliding(PhysicalObject collidingObject){

        return ((this.hitbox.isColliding(collidingObject.getHitbox()) && !(collidingObject instanceof Lemming))
        || this.footHitbox.isColliding(collidingObject.getHitbox()) && !(collidingObject instanceof Lemming)
        || collidingObject instanceof Lemming && this.hitbox.isColliding(((Lemming)collidingObject).getHitbox()) && ((Lemming) collidingObject).hasProperty("block"));

    }

    /**
     * Normal behavior of a lemming when falling
     * @param lambda as double
     */
    public void updateNoSkill(double lambda){

        Vector normalizedSpeed = new Vector();
        Vector normalizedAcceleration = new Vector();
        Vector sumOfForces = this.getSumForces();

        normalizedAcceleration.setCoordinates(sumOfForces.getX() * lambda, sumOfForces.getY() * lambda);
        this.setSpeed(this.speed.getX() + normalizedAcceleration.getX(), this.speed.getY() + normalizedAcceleration.getY());

        normalizedSpeed.setCoordinates(this.speed.getX() * lambda, this.speed.getY()* lambda);
        this.setPosition(this.position.getX() + normalizedSpeed.getX(), this.position.getY() + normalizedSpeed.getY());

        this.hitbox.move(new Vector(hitbox.getCenter().getX() + normalizedSpeed.getX(), hitbox.getCenter().getY() + normalizedSpeed.getY()));
        this.footHitbox.move(new Vector(footHitbox.getCenter().getX() + normalizedSpeed.getX(), footHitbox.getCenter().getY() + normalizedSpeed.getY()));

    }

    /**
     * To normally make a lemming fall
     * @param lambda the time between this update and the last one ( 1 / framerate )
     */
    @Override
    public void update(double lambda){
        if (skill == null) updateNoSkill(lambda);
        else skill.update(this, lambda);
    }

    public void teleport(double x, double y){
        this.hitbox.move(x, y + hitbox.getCenter().getY() - getPosition().getY());
        this.footHitbox.move(x, y + footHitbox.getCenter().getY() - getPosition().getY());
        this.setPosition(x, y);
    }

    public boolean canTeleport(double x, double y){

        boolean willCollide = true;

        LinkedList<PhysicalObject> closeObjects = currentLevel.getBlocksFromRadius(hitbox.getCenter(), (int) Math.sqrt(image.getHeight()/2 * image.getHeight()/2
        + image.getWidth()/2 * image.getWidth()/2) + 10);

        Hitbox tempHitbox = new Hitbox(x - 1, y - image.getHeight()/2,
                x + 1,y);

        closeObjects.remove(this);
        for(PhysicalObject object : closeObjects){
            if(tempHitbox.isColliding(object.getHitbox())){
                willCollide = false;
            }
        }

        return willCollide;

    }


    private void createImage() {
        currentAnimation = walkAnimation;
        this.image = walkAnimation.getCurrentSprite().getImg();
        walkAnimation.setStatus(0);
    }

    public String toString(){//Todo
        return "Lemming is at " + this.position.toString();
    }

    public boolean equals(){//Todo
        return true;
    }

    public Image getImage(){
        if (skill != null) return skill.getImage();
        return this.currentAnimation.getCurrentSprite().getImg();
    }

    public Hitbox getFootHitbox() {
        return footHitbox;
    }
    public boolean isGrounded(){
        //return this.grounded;
        return this.speed.getY() == 0;
    }

    public void setSkill(Skill skill) {
        if (this.skill == null || skill == null || skill.canOvertake()) {
            if (this.skill != null && (skill != null && skill.canOvertake())) this.skill.close(this);
            this.skill = skill;
        }
    }

    public void setSkill(SkillBuilder builder){
        if (this.skill == null || builder.canOvertake()) {
            if (this.skill != null && builder.canOvertake()) this.skill.close(this);
            this.skill = builder.toSkill();
        }
    }

    public List<String> getProperties() {
        return properties;
    }

    public boolean hasProperty(String property){
        return this.properties.contains(property);
    }

    public PhysicalWorld getPhysicalWorld(){
        return currentLevel.getPhysicalWorld();
    }

    public Level getCurrentLevel() {
        return currentLevel;
    }

    public int translateConstant() {
        return - horizontalDirection;
    }


    public int getHorizontalDirection() {
        return horizontalDirection;
    }

    public Skill getSkill() {
        return skill;
    }

    public int getHorizontalSpeed() {
        return horizontalSpeed;
    }

    public Animation getFallAnimation(){
        return this.fallAnimation;
    }

    public Animation getWalkAnimation(){
        return this.walkAnimation;
    }
    public void setCurrentAnimation(Animation animation){
        this.currentAnimation = animation;
    }

    public void setHorizontalDirection(int horizontalDirection) {
        this.horizontalDirection = horizontalDirection;
    }
}
