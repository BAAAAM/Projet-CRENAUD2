package crenaud2.lemmings;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import crenaud2.cinematics.Sprite;
import javafx.application.Platform;
import javafx.scene.image.Image;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by aethor on 02/06/17.
 */
public class Animation {

    private List<Sprite> sprites;
    private double framerate;
    private boolean reversed;
    private boolean looped;
    private int status;
    private int call;
    private Sprite currentSprite;

    public Animation(){

        generateAnimation(null, 0.0, false, true, 0);

    }

    public Animation(ArrayList<Sprite> sprites){

        generateAnimation(sprites, sprites.size(), false, true, 0);

    }

    public Animation(ArrayList<Sprite> sprites, double framerate, boolean reversed, boolean looped){

        generateAnimation(sprites, framerate, reversed, looped, 0);

    }

    public Animation(ArrayList<Sprite> sprites, double framerate, boolean reversed, boolean looped, int status){

        generateAnimation(sprites, framerate, reversed, looped, status);

    }

    private void generateAnimation(List<Sprite> sprites, double framerate, boolean reversed, boolean looped, int status){

        this.sprites = sprites;
        this.framerate = framerate;
        if(sprites.size() == 0){
            throw new IllegalStateException();
        }
        this.framerate = framerate;
        if(this.framerate < 0){
            throw new ArithmeticException();
        }
        this.reversed = reversed;
        this.looped = looped;
        if(status > sprites.size() || status < 0){
            throw new IndexOutOfBoundsException();
        }
        else{
            this.status = status;
        }


    }

    public Animation(String path){
        this(path, false);
    }

    public Animation(String path, boolean isLemming){
        Gson g = new Gson();
        JsonReader reader = g.newJsonReader(new InputStreamReader(getClass().getResourceAsStream(path)));
        String name = null;
        List<Sprite> sprites = new ArrayList<>();
        double framerate = 0.0;
        boolean reversed = false;
        boolean looped = true;
        int status = 0;

        try {
            reader.beginObject();
            while (reader.hasNext()){
                name = reader.nextName();
                switch (name) {
                    case "status":
                        status = reader.nextInt();
                        break;
                    case "looped":
                        looped = reader.nextBoolean();
                        break;
                    case "reversed":
                        reversed = reader.nextBoolean();
                        break;
                    case "framerate":
                        framerate = reader.nextDouble();
                        break;
                    case "sprites":
                        reader.beginArray();
                        while (reader.hasNext())
                            if (isLemming)
                                sprites.add(new Sprite(reader.nextString(),
                                        LemmingConstants.LEMMING_WIDTH, LemmingConstants.LEMMING_HEIGHT,
                                        LemmingConstants.RATIO, LemmingConstants.SMOOTH));
                            else sprites.add(new Sprite(reader.nextString()));
                        reader.endArray();
                        break;
                    default:
                        reader.skipValue();
                        break;
                }
            }
            reader.endObject();
        } catch (IOException e) {
            e.printStackTrace();
        }
        generateAnimation(sprites, framerate, reversed, looped, status);
    }

    /**
     * get the sprite to draw
     * @return a Sprite object
     */
    public Sprite getCurrentSprite(){

        if (++call >= framerate || currentSprite == null) {
            call = 0;
            if (!reversed) {
                currentSprite = getNext();
            } else {
                currentSprite = getPrevious();
            }
        }
        return currentSprite;

    }

    private Sprite getNext(){
        if(this.status == sprites.size() - 1){
            if(this.looped){
                this.status = 0;
                return sprites.get(0);
            }
            else {
                this.status = -1;
                return null;
            }
        }
        else{
            ++status;
            return sprites.get(status);
        }
    }

    private Sprite getPrevious(){
        if(this.status == 0){
            if(this.looped){
                this.status = sprites.size() - 1;
                return this.sprites.get(sprites.size() - 1);
            }
            else {
                this.status = -1;
                return null;
            }
        }
        else{
            --status;
            return this.sprites.get(status);
        }

    }

    public Animation clone(){
        return new Animation((ArrayList<Sprite>) this.sprites, this.framerate, this.reversed, this.looped, this.status);
    }

    public void setSprites(ArrayList<Sprite> sprites) {
        this.sprites = sprites;
    }

    public void setFramerate(double framerate) {
        if (framerate >= 0) this.framerate = framerate;
    }

    public boolean isReversed() {
        return reversed;
    }

    public void setReversed(boolean reversed) {
        this.reversed = reversed;
    }

    public boolean isLooped() {
        return looped;
    }

    public void setLooped(boolean looped) {
        this.looped = looped;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

}
