package crenaud2.lemmings;

/**
 * Created by klmp200 on 23/06/2017.
 */
public class LemmingConstants {
    public static int LEMMING_WIDTH = 15 * 2;
    public static int LEMMING_HEIGHT = 45 * 5;
    public static boolean RATIO = true;
    public static boolean SMOOTH = true;
}
