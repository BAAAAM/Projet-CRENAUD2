package crenaud2.lemmings.skills;

import crenaud2.lemmings.Animation;
import crenaud2.lemmings.Lemming;
import crenaud2.map.Block;
import crenaud2.physics.PhysicalObject;

/**
 * Created by aethor on 26/06/17.
 */
public class DiggerSkill extends Skill{

    private int uses;
    private Animation standardAnimation;

    public DiggerSkill(){
        super();
        animation = new Animation("/json/animations/diggerAnimation.json", true);
        standardAnimation = animation;
        uses = 40;
    }

    @Override
    public void update(Lemming lemming, double lambda) {
        if(!canUse()){
            this.close(lemming);
        }
        lemming.updateNoSkill(lambda);
    }

    /**
     * Called when the lemming is colliding
     * @param lemming a Lemming
     * @param collidingObject a PhysicalObject
     */
    @Override
    public void collide(Lemming lemming, PhysicalObject collidingObject) {

        if(animation != standardAnimation){
            animation = standardAnimation;
        }

        if(collidingObject instanceof Block && ((Block) collidingObject).hasProperty("end")){//when lemming hit an "end" object, we consider that he's saved
            lemming.getCurrentLevel().removeLemming(lemming, true);
        }

        lemming.setSpeed(0,0);
        lemming.addUniqForce("antiGravity", lemming.getForces().get("gravity").negate());

        if(canUse()){

            lemming.getCurrentLevel().deleteObject(collidingObject);
            uses--;

        }

        else{
            this.close(lemming);
        }

    }

    /**
     * to call when the lemming is not colliding with anything
     * @param lemming a Lemming
     */
    @Override
    public void notCollided(Lemming lemming) {
        lemming.notCollidedNoSkill();
    }

    @Override
    public boolean canUse() {
        return (uses > 0);
    }

    @Override
    public void close(Lemming lemming) {
        lemming.setSkill((Skill) null);
    }
}
