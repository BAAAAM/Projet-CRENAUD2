package crenaud2.lemmings.skills;

import crenaud2.lemmings.Animation;
import crenaud2.lemmings.Lemming;
import crenaud2.map.Block;
import crenaud2.physics.PhysicalObject;
import crenaud2.physics.Vector;

/**
 * Created by klmp200 on 23/06/2017.
 */
public class BlockerSkill extends Skill {

    private boolean stopped;

    public BlockerSkill() {
        super();
        animation = new Animation("/json/animations/blockerAnimation.json", true);
        stopped = false;
    }

    /**
     * To call when a normal walk is required
     *
     * @param lemming a Lemming
     * @param lambda  a double
     */
    @Override
    public void update(Lemming lemming, double lambda) {
        if(!stopped){
            lemming.updateNoSkill(lambda);
        }
    }

    /**
     * To call when the lemming is colliding
     *
     * @param lemming         a Lemming
     * @param collidingObject a PhysicalObject
     */
    @Override
    public void collide(Lemming lemming, PhysicalObject collidingObject) {

        if(collidingObject instanceof Block && ((Block) collidingObject).hasProperty("end")){//when lemming hit an "end" object, we consider that he's saved
            lemming.getCurrentLevel().removeLemming(lemming, true);
        }
        lemming.setSpeed(0, 0);
        lemming.addUniqForce("antiGravity", lemming.getForces().get("gravity").negate());
        if (!stopped) lemming.getProperties().add("block");
        stopped = true;

    }

    /**
     * To call when the lemming is not colliding with anything
     *
     * @param lemming a Lemming
     */
    @Override
    public void notCollided(Lemming lemming) {
        if(!stopped) lemming.notCollidedNoSkill();
    }

    /**
     * Check if the skill can be used
     *
     * @return boolean
     */
    @Override
    public boolean canUse() {
        return true;
    }

    /**
     * Correctly close the skill
     *
     * @param lemming a Lemming
     */
    @Override
    public void close(Lemming lemming) {
        lemming.getProperties().remove("block");
    }
}
