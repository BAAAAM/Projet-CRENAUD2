package crenaud2.lemmings.skills;

/**
 * Created by klmp200 on 10/05/2017.
 */
public class SkillFinder {
    public static final Skill findSkill(String skill){
        switch (skill){
            case "stairs": return new StairsBuilderSkill();
            case "blocker": return new BlockerSkill();
            case "digger": return new DiggerSkill();
            case "horizontalDigger": return new HorizontalDiggerSkill();
            case "suicide": return new SuicideSkill();
            default: return null;
        }
    }
}
