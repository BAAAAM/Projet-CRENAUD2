package crenaud2.lemmings.skills;

import crenaud2.cinematics.Sprite;
import crenaud2.lemmings.Animation;
import crenaud2.lemmings.Lemming;
import crenaud2.lemmings.LemmingConstants;
import crenaud2.map.Stair;
import crenaud2.physics.Hitbox;
import crenaud2.physics.PhysicalObject;
import crenaud2.physics.Vector;
import javafx.scene.image.Image;

import java.io.FileNotFoundException;

/**
 * Created by klmp200 on 10/05/2017.
 */
public class StairsBuilderSkill extends Skill {

    private static int usesMax;
    private int uses;
    private boolean usedAtThisStage;//ensure the lemming can only use one stair by height
    private Animation standardAnimation;

    public StairsBuilderSkill() {
        super();
        usedAtThisStage = false;
        animation = new Animation("/json/animations/builderAnimation.json", true);
        standardAnimation = animation;
        usesMax = 20;
        uses = 0;
    }

    /**
     * To call when a normal fall is required
     *
     * @param lemming a Lemming
     * @param lambda  a double
     */
    @Override
    public void update(Lemming lemming, double lambda) {
        lemming.updateNoSkill(lambda);
    }

    /**
     * To call when the lemming is colliding
     *
     * @param lemming         a Lemming
     * @param collidingObject a PhysicalObject
     */
    @Override
    public void collide(Lemming lemming, PhysicalObject collidingObject) {

        lemming.collideNoSkill(collidingObject);

        if(animation != standardAnimation){
            animation = standardAnimation;
        }

        if (this.canUse() && lemming.getFootHitbox().isColliding(collidingObject.getHitbox()) &&
                lemming.getPosition().getX() * lemming.getHorizontalDirection() > collidingObject.getPosition().getX() * lemming.getHorizontalDirection()){

            Vector stairPosition = new Vector(
           lemming.getPosition().getX() + 12 * lemming.getHorizontalDirection(),
                    lemming.getFootHitbox().getCenter().getY());

            Image stairImage = new Image("/img/tilesets/stairs.png");

            Hitbox stairHitbox = new Hitbox(stairPosition.getX() - stairImage.getWidth()/2,
                    stairPosition.getY() - stairImage.getHeight()/2,
                    stairPosition.getX() + stairImage.getWidth()/2,
                    stairPosition.getY() + stairImage.getHeight()/2);

            if(lemming.canTeleport(lemming.getPosition().getX() + 1 * lemming.getHorizontalDirection(),
                    lemming.getPosition().getY() - stairHitbox.getYmax() + stairHitbox.getYmin())){


                Stair stair = new Stair(stairImage, stairHitbox, stairPosition);

                lemming.getPhysicalWorld().delayedAddToObjectList(stair);
                lemming.getCurrentLevel().getStairs().add(stair);

                uses++;
                usedAtThisStage = true;

                if(uses == usesMax){
                    close(lemming);
                }

            }else{

                close(lemming);
                lemming.setHorizontalDirection(-lemming.getHorizontalDirection());

            }

        }


    }

    /**
     * To call when the lemming is not colliding with anything
     *
     * @param lemming a Lemming
     */
    @Override
    public void notCollided(Lemming lemming) {
        animation = lemming.getFallAnimation();
        lemming.notCollidedNoSkill();
    }

    /**
     * Check if the skill can be used
     * @return boolean
     */
    @Override
    public boolean canUse() {
        return (uses < usesMax && !usedAtThisStage);
    }

    /**
     * Correctly close the skill
     * @param lemming a Lemming
     */
    @Override
    public void close(Lemming lemming) {
        lemming.setSkill((Skill) null);
    }

    public void setUsedAtThisStage(boolean bool){
        this.usedAtThisStage = bool;
    }

}
