package crenaud2.lemmings.skills;

import crenaud2.lemmings.Animation;
import crenaud2.lemmings.Lemming;
import crenaud2.map.Block;
import crenaud2.physics.PhysicalObject;

/**
 * Created by aethor on 27/06/17.
 */
public class HorizontalDiggerSkill extends Skill{

    private int uses;
    private Animation standardAnimation;

    public HorizontalDiggerSkill(){
        super();
        animation = new Animation("/json/animations/diggerAnimation.json", true);
        standardAnimation = animation;
        uses = 70;
    }
    /**
     * To call when a normal walk is required
     *
     * @param lemming a Lemming
     * @param lambda  a double
     */
    @Override
    public void update(Lemming lemming, double lambda) {

        if(!canUse()){
            this.close(lemming);
        }

        lemming.updateNoSkill(lambda);

    }

    /**
     * To call when the lemming is colliding
     *
     * @param lemming         a Lemming
     * @param collidingObject a PhysicalObject
     */
    @Override
    public void collide(Lemming lemming, PhysicalObject collidingObject) {

        if(standardAnimation != animation){
            animation = standardAnimation;
        }
        if(collidingObject instanceof Block && ((Block) collidingObject).hasProperty("end")){//when lemming hit an "end" object, we consider that he's saved
            lemming.getCurrentLevel().removeLemming(lemming, true);
        }

        lemming.setSpeed(lemming.getHorizontalDirection() * lemming.getHorizontalSpeed(),0);
        lemming.addUniqForce("antiGravity", lemming.getForces().get("gravity").negate());

        if(canUse() && lemming.getHitbox().isColliding(collidingObject.getHitbox())){

            lemming.getCurrentLevel().deleteObject(collidingObject);
            uses--;

        }

    }

    /**
     * To call when the lemming is not colliding with anything
     *
     * @param lemming a Lemming
     */
    @Override
    public void notCollided(Lemming lemming) {

        animation = lemming.getFallAnimation();
        lemming.notCollidedNoSkill();

    }

    /**
     * Check if the skill can be used
     *
     * @return boolean
     */
    @Override
    public boolean canUse() {
        return(uses > 0);
    }

    /**
     * Correctly close the skill
     *
     * @param lemming a Lemming
     */
    @Override
    public void close(Lemming lemming) {

        lemming.setSkill((Skill) null);

    }
}
