package crenaud2.lemmings.skills;

/**
 * Created by klmp200 on 10/05/2017.
 */

import crenaud2.lemmings.Lemming;
import crenaud2.physics.PhysicalObject;

/**
 * Abstract skill, should be converted to a real Skill
 */
public class SkillBuilder {
    private String name;
    private int quantity;
    private boolean canOvertake;

    /**
     * Create an SkillBuilder
     * @param name String
     * @param quantity int
     */
    public SkillBuilder(String name, int quantity) {
        Skill tempSkill = null;
        this.name = name;
        this.quantity = quantity + 1;
        tempSkill = this.toSkill();
        canOvertake = tempSkill.canOvertake();
    }

    /**
     * Get name of the skill
     * @return String
     */
    public String getName() {
        return name;
    }

    /**
     * Get the available quantity
     * @return int
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * Get the corresponding skill if they are some left
     */
    public Skill toSkill(){
        if (isUsable()){
            --quantity;
            return SkillFinder.findSkill(name);
        } else return null;
    }

    /**
     * Check if skill is still available to use
     * @return boolean
     */
    public boolean isUsable() {
        return this.quantity > 0;
    }

    /**
     * If a skill can replace another skill
     * @return boolean
     */
    public boolean canOvertake() {
        return canOvertake;
    }

    /**
     * Get the name with a capitalized first letter
     * @return String
     */
    public String getDisplayName(){
        return name.substring(0,1).toUpperCase() + name.substring(1).toLowerCase() + " : " + quantity;
    }

    @Override
    public String toString() {
        return "SkillBuilder{" +
                "name='" + name + '\'' +
                ", quantity=" + quantity +
                '}';
    }
}
