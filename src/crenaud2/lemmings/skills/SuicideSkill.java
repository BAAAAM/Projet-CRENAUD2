package crenaud2.lemmings.skills;

import crenaud2.lemmings.Animation;
import crenaud2.lemmings.Lemming;
import crenaud2.map.Block;
import crenaud2.physics.PhysicalObject;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 * Created by aethor on 27/06/17.
 */
public class SuicideSkill extends Skill{

    private double countdown;

    public SuicideSkill(){
        super();
        countdown = 4;
        animation = new Animation("/json/animations/walk.json", true);
    }


    @Override
    public void update(Lemming lemming, double lambda) {

        lemming.updateNoSkill(lambda);

        countdown -= lambda;

        if(countdown <= 0){

            LinkedList<PhysicalObject> objects = lemming.getCurrentLevel().getObjectFromRadius(lemming.getPosition() , 50);

            objects.forEach(object -> {
                if (!(object instanceof Lemming) || object == lemming) {
                    lemming.getCurrentLevel().deleteObject(object);
                }
            });

            lemming.getCurrentLevel().removeLemming(lemming, false);

        }

    }

    @Override
    public void collide(Lemming lemming, PhysicalObject collidingObject) {

        lemming.collideNoSkill(collidingObject);

    }

    @Override
    public void notCollided(Lemming lemming) {

        lemming.notCollidedNoSkill();

    }

    @Override
    public boolean canUse() {
        return true;
    }

    @Override
    public void close(Lemming lemming) {

    }

    @Override
    public boolean canOvertake(){ return true; }

    public double getCountdown(){
        return this.countdown;
    }
}
