package crenaud2.lemmings.skills;

import crenaud2.cinematics.Sprite;
import crenaud2.lemmings.Animation;
import crenaud2.lemmings.Lemming;
import crenaud2.physics.PhysicalObject;
import crenaud2.physics.PhysicalWorld;
import javafx.scene.image.Image;

import java.io.FileNotFoundException;

/**
 * Created by klmp200 on 10/05/2017.
 */
public abstract class Skill {

    protected String name;
    protected Animation animation;


    /**
     * To call when a normal walk is required
     * @param lemming a Lemming
     * @param lambda a double
     */
    public abstract void update(Lemming lemming, double lambda);

    /**
     * To call when the lemming is colliding
     * @param lemming a Lemming
     * @param collidingObject a PhysicalObject
     */
    public abstract void collide(Lemming lemming, PhysicalObject collidingObject);

    /**
     * To call when the lemming is not colliding with anything
     * @param lemming a Lemming
     */
    public abstract void notCollided(Lemming lemming);

    /**
     * Check if the skill can be used
     * @return boolean
     */
    public abstract boolean canUse();

    /**
     * Get the sprite to display
     * @return Sprite
     */
    public Image getImage(){
        return animation.getCurrentSprite().getImg();
    }

    /**
     * If a skill can replace another skill
     * @return boolean
     */
    public boolean canOvertake() { return false; }

    /**
     * Correctly close the skill
     * @param lemming a Lemming
     */
    public abstract void close(Lemming lemming);

}
