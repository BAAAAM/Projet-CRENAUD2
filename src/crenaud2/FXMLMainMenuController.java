package crenaud2;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.util.Duration;

import java.io.IOException;

/**
 * Created by klmp200 on 25/06/2017.
 */
public class FXMLMainMenuController implements FXMLController {

    private Crenaud2 app;
    private MediaPlayer music;
    private MediaPlayer startMusic;
    private MediaPlayer buttonMusic;

    @FXML Button newGameButton;
    @FXML Button loadSaveButton;
    @FXML TextField loadSaveTextField;
    @FXML Label errorLabel;

    /**
     * Should load a json from a given path and launch the game
     *
     * @param json as String
     */
    @Override
    public void loadJson(String json) throws IOException {
        buttonMusic = new MediaPlayer(new Media(getClass().getClassLoader().getResource("sound/OST/Button.wav").toExternalForm()));
        buttonMusic.setOnEndOfMedia(new Runnable() {
            @Override
            public void run() {
                realStart();
            }
        });
        startMusic = new MediaPlayer(new Media(getClass().getClassLoader().getResource("sound/OST/Game_launch.wav").toExternalForm()));
        startMusic.setOnEndOfMedia(new Runnable() {
            @Override
            public void run() {
                music.play();
            }
        });
        music = new MediaPlayer(new Media(getClass().getClassLoader().getResource("sound/OST/Menu_Strange.wav").toExternalForm()));
        music.setOnEndOfMedia(new Runnable() {
            @Override
            public void run() {
                music.seek(Duration.ZERO);
            }
        });
        startMusic.play();
    }

    /**
     * To launch after playing a nice song
     */
    private void realStart() {
        try {
            app.nextLevel();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Unload everything (like music)
     */
    @Override
    public void clean() {
        music.stop();
        startMusic.stop();
    }

    /**
     * Set a reference to the main programme
     *
     * @param app Crenaud2
     */
    @Override
    public void setApplication(Crenaud2 app) {
        this.app = app;
    }

    /**
     * Set the code to display to reach this level
     *
     * @param code
     */
    @Override
    public void setCode(String code) {
        /* we do nothing, we don't care */
    }

    @FXML private void loadSave(){
        if (app.hasGivenLevelCode(loadSaveTextField.getText())){
            try {
                app.nextLevel(loadSaveTextField.getText());
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            loadSaveTextField.getStyleClass().add("error");
            errorLabel.setText("No corresponding level");
        }
    }

    @FXML private void newGame(){
        music.stop();
        startMusic.stop();
        buttonMusic.play();
    }
}
