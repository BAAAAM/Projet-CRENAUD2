package crenaud2.gameplay;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import crenaud2.lemmings.Lemming;
import crenaud2.lemmings.skills.SkillBuilder;
import crenaud2.map.Tilemap;
import javafx.scene.media.Media;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by klmp200 on 10/05/2017.
 */

/**
 * Build a level from a json
 * Should be accessed from Level
 */
public class LevelBuilder {

    private int lemmingsToSpawn;
    private int lemmingsToSave;
    private String name;
    private List<SkillBuilder> skills;
    private List<Media> musics;
    private Tilemap tilemap;
    private double spawnRate;

    /**
     * Create a new LevelBuilder from a json file
     * @param path json file as String
     * @throws IOException if error occurs reading a file
     */
    public LevelBuilder(String path) throws IOException {
        Gson g = new Gson();
        JsonReader reader = g.newJsonReader(new InputStreamReader(getClass().getResourceAsStream(path)));
        String name = null;
        this.skills = new ArrayList<>();
        this.musics = new ArrayList<>();
        this.spawnRate = 0.5;

        reader.beginObject();
        while(reader.hasNext()){
            name = reader.nextName();
            if (name.equals("name"))
                this.name = reader.nextString();
            else if (name.equals("tilemap"))
                this.tilemap = new Tilemap(reader.nextString());
            else if (name.equals("lemmings to spawn"))
                this.lemmingsToSpawn = reader.nextInt();
            else if (name.equals("lemmings to save"))
                this.lemmingsToSave = reader.nextInt();
            else if (name.equals("musics"))
                this.parseMusics(reader);
            else if (name.equals("skills"))
                this.parseSkills(reader);
            else if (name.equals("spawn rate"))
                this.spawnRate = reader.nextDouble();
            else
                reader.skipValue();
        }
        reader.endObject();

    }

    /**
     * Generate the list of abstract skills
     * @param reader JsonReader
     * @throws IOException if error while reading the file
     */
    private void parseSkills(JsonReader reader) throws IOException {
        String name = null;
        String skillName;
        Integer uses;
        reader.beginArray();

        while (reader.hasNext()){
            skillName = null;
            uses = null;

            reader.beginObject();
            while (reader.hasNext()) {
                name = reader.nextName();
                if (name.equals("name"))
                    skillName = reader.nextString();
                else if (name.equals("uses"))
                    uses = reader.nextInt();
                else
                    reader.skipValue();
            }
            if (uses != null && skillName != null)
                skills.add(new SkillBuilder(skillName, uses));
            reader.endObject();
        }

        reader.endArray();

    }

    /**
     * Fill the list of musics from the json file
     * @param reader JsonReader
     * @throws IOException if error while reading the file
     */
    private void parseMusics(JsonReader reader) throws IOException {
        Media music = null;
        reader.beginArray();

        while (reader.hasNext()) {
            try {
                music = new Media(getClass().getClassLoader().getResource(reader.nextString()).toExternalForm());
            } catch (Exception e){
                e.printStackTrace();
                music = null;
            }
            musics.add(music);
        }

        reader.endArray();
    }

    /**
     * Get the name of the level
     * @return String
     */
    public String getName() {
        return name;
    }

    /**
     * Get the list of skills (Abstract) available on this level
     * @return List<SkillBuilder>
     */
    public List<SkillBuilder> getSkills() {
        return skills;
    }

    /**
     * Get the list of musics available on this level
     * @return List<Media>
     */
    public List<Media> getMusics() {
        return musics;
    }

    /**
     * Get the tilemap
     * @return Tilemap
     */
    public Tilemap getTilemap() {
        return tilemap;
    }

    /**
     * Get the number of lemmings to spawn
     * @return integer
     */
    public int getLemmingsToSpawn() {
        return lemmingsToSpawn;
    }

    /**
     * Get the number of lemmings needed to win
     * @return integer
     */
    public int getLemmingsToSave() {
        return lemmingsToSave;
    }

    @Override
    public String toString() {
        return "LevelBuilder{" +
                "lemmingsToSpawn=" + lemmingsToSpawn +
                ", lemmingsToSave=" + lemmingsToSave +
                ", name='" + name + '\'' +
                ", skills=" + skills +
                ", musics=" + musics +
                ", tilemap=" + tilemap +
                '}';
    }

    public double getSpawnRate() {
        return spawnRate;
    }
}
