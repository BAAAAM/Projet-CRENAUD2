package crenaud2.gameplay;

import crenaud2.Crenaud2;
import crenaud2.lemmings.Animation;
import crenaud2.lemmings.Lemming;
import crenaud2.lemmings.skills.SkillBuilder;
import crenaud2.lemmings.skills.SuicideSkill;
import crenaud2.map.Block;
import crenaud2.map.Stair;
import crenaud2.map.Tilemap;
import crenaud2.physics.Hitbox;
import crenaud2.physics.PhysicalObject;
import crenaud2.physics.PhysicalWorld;
import crenaud2.physics.Vector;
import javafx.scene.image.Image;
import javafx.scene.media.Media;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by klmp200 on 10/05/2017.
 */

/**
 * Create and handle a Level from a json
 */
public class Level {
    //NUTHIN

    private LevelBuilder builder;
    private ArrayList<Stair> stairs;
    private ArrayList<Lemming> aliveLemmings;
    private boolean pause;
    private PhysicalWorld physicalWorld;
    private double spawnRate;
    private double timeElapsed;
    private double timeSinceLastSpawn;
    private int lemmingsSpawned;
    private int lemmingsSaved;
    private Animation lemmingWalkAnimation;
    private Animation lemmingFallAnimation;
    private Crenaud2 app;
    private boolean spawnKill;
    private boolean exited;

    public Level(String json, Crenaud2 app) throws IOException {

        this.builder = new LevelBuilder(json);//load level from json hehehehe
        stairs = new ArrayList<>();
        this.pause = false;
        this.aliveLemmings = new ArrayList<Lemming>();
        this.physicalWorld = new PhysicalWorld();
        this.spawnRate = builder.getSpawnRate();
        this.timeElapsed = 0.0;
        timeSinceLastSpawn = 0.0;
        lemmingsSpawned = 0;

        //Add every physical block ( layer 0 ) to the physical world
        for(int i = 1; i < this.builder.getTilemap().getLayers().size(); i++){
            this.builder.getTilemap().getLayers().get(i).getBlocks().forEach(block -> {
                if(block.hasPhysicalBody){
                    this.physicalWorld.getObjectList().add(block);
                }
            });
        }

        createLemmingAnimation();//create the lemming walking animation only once to prevent too much loading at lemming spawning

        this.app = app;

    }

    public void spawnLemming(double lambda){

        this.timeElapsed += lambda;
        this.timeSinceLastSpawn += lambda;

        if(lemmingsSpawned < getLemmingsToSpawn() && timeSinceLastSpawn >= 1/spawnRate){
            try {
                Lemming tempLemming = new Lemming(150, 50, this); //TODO : hardcoded atm
                if (spawnKill) tempLemming.setSkill(new SuicideSkill());
                this.aliveLemmings.add(tempLemming);
                this.physicalWorld.getObjectList().add(tempLemming);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            timeSinceLastSpawn = 0.0;
            lemmingsSpawned++;
        }

    }

    public void destroy(){
        getMap().getLayers().forEach( layer -> {
            layer.getBlocks().forEach(block -> {
                block = null;
            });
        });
        getMap().getLayers().forEach( layer -> {
            layer = null;
        });
        getAliveLemmings().forEach(lemming -> {
            lemming = null;
        });
    }

    private void createLemmingAnimation(){
        lemmingWalkAnimation = new Animation("/json/animations/walk.json", true);
        lemmingFallAnimation = new Animation("/json/animations/blockerAnimation.json", true);
    }


    public String getName(){
        return builder.getName();
    }

    public Tilemap getMap(){
        return builder.getTilemap();
    }

    public List<Media> getMusics(){
        return builder.getMusics();
    }

    public ArrayList<Lemming> getAliveLemmings(){
        return this.aliveLemmings;
    }

    public int getLemmingsToSpawn(){
        return this.builder.getLemmingsToSpawn();
    }

    public int getLemmingsToSave(){
        return this.builder.getLemmingsToSave();
    }

    public void autoPause(){pause = !pause;}

    public int getLemmingsSaved(){
        return this.lemmingsSaved;
    }

    /**
     * remove a lemming from a level, and launch the end level function if needed
     * @param lemming the lemming to remove
     */
    public void removeLemming(Lemming lemming, boolean isSaved){

        getAliveLemmings().remove(lemming);
        lemming = null;

        if(isSaved){
            lemmingsSaved++;
        }
        if(aliveLemmings.size() == 0){

            endLevel(lemmingsSaved >= getLemmingsToSave());

        }

    }


    /**
     * End a level, and launch the next one
     * @param isSuccesfull
     */
    public void endLevel(boolean isSuccesfull){

        if (!exited) {
            System.out.println(isSuccesfull);
            try {
                exited = true;
                app.nextLevel(isSuccesfull);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * Update the whole level
     * @param lambda as double
     */
    public void update(double lambda){
        spawnLemming(lambda);
        physicalWorld.update(lambda);
    }

    public List<SkillBuilder> getAvailableSkills() {
        return builder.getSkills();
    }

    public PhysicalWorld getPhysicalWorld(){
        return physicalWorld;
    }

    public void setSkillOnLemming(SkillBuilder builder, double x, double y){
        Lemming l = getAliveLemmings().stream().filter(lemming ->
                lemming.getHitbox().pointIsInHitbox(x, y)
        ).findAny().orElse(null);
        if (l != null) {
            l.setSkill(builder);
        }
    }

    public ArrayList<Stair> getStairs(){
        return this.stairs;
    }

    public Animation getLemmingWalkAnimation() {
        return lemmingWalkAnimation;
    }

    /**
     * delete physical object from a level, removing him both from the physical world and the displaying loop
     * @param object
     */
    public void deleteObject(PhysicalObject object){
        physicalWorld.delayedRemoveFromObjectList(object);
        if(object instanceof Stair){
            stairs.remove(object);
        }else{
            getMap().getLayers().forEach(layer -> {
                layer.getBlocks().remove(object);
            });
        }

    }

    public LinkedList<PhysicalObject> getObjectFromRadius(Vector center, int radius) {

        LinkedList<PhysicalObject> objects = new LinkedList<>();

        getMap().getLayers().forEach(layer -> {
            layer.getBlocks().forEach(block -> {
                if (block.getPosition().distance(center) <= radius && block.hasPhysicalBody){
                    objects.add(block);
                }
            });
        });

        aliveLemmings.forEach(lemming -> {
            if (lemming.getPosition().distance(center) <= radius){
                objects.add(lemming);
            }
        });

        stairs.forEach(stair -> {
            if(stair.getPosition().distance(center) <= radius){
                objects.add(stair);
            }
        });

        return objects;
    }

    public LinkedList<PhysicalObject> getBlocksFromRadius(Vector center, int radius){

        LinkedList<PhysicalObject> objects = new LinkedList<>();

        getMap().getLayers().forEach(layer -> {
            layer.getBlocks().forEach(block -> {
                if (block.getPosition().distance(center) <= radius && block.hasPhysicalBody){
                    objects.add(block);
                }
            });
        });

        return objects;
    }

    public Animation getLemmingFallAnimation() {
        return lemmingFallAnimation;
    }

    /**
     * To kill every living lemmings
     */
    public void killEveryone(){
        spawnKill = true;
        getAliveLemmings().forEach(lemming -> lemming.setSkill(new SuicideSkill()));
    }
}
