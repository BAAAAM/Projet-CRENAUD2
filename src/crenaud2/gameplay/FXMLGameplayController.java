package crenaud2.gameplay;

/**
 * Created by klmp200 on 08/05/2017.
 */

import crenaud2.Crenaud2;
import crenaud2.FXMLController;
import crenaud2.lemmings.skills.SkillBuilder;
import crenaud2.lemmings.skills.SuicideSkill;
import crenaud2.physics.Hitbox;
import javafx.animation.PauseTransition;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.*;
import javafx.scene.effect.Reflection;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.media.MediaPlayer;
import javafx.scene.shape.ArcType;
import javafx.util.Duration;
import ui.ResizableCanvas;

import java.io.IOException;
import java.util.Date;

/**
 * Control the display of an actual levels
 */
public class FXMLGameplayController implements FXMLController {

    private Level current;
    private Long time1;
    private Crenaud2 app;
    private PauseTransition displayLoop;
    private MediaPlayer music;
    private boolean killEveryone;

    @FXML private ResizableCanvas gameDisplay;
    @FXML private ToolBar skillBar;
    @FXML private ToolBar menuBar;
    @FXML private ScrollPane mainPane;
    @FXML private Label codeLabel;
    @FXML private Label lemmingsToSaveLabel;

    /**
     * Load a new Level from a json
     * @param json as String
     * @throws IOException if error while reading files
     */
    public void loadJson(String json) throws IOException {
        this.current = new Level(json, app);
        this.prepareWindow();
    }

    /**
     * Set all listeners and background tasks
     */
    private void prepareWindow(){
        this.displayLoop = new PauseTransition(Duration.seconds(0.03));

        gameDisplay.widthProperty().bind(mainPane.widthProperty());
        gameDisplay.heightProperty().bind(mainPane.heightProperty());
        time1 = new Long((new Date()).getTime());

        lemmingsToSaveLabel.setText(Integer.toString(current.getLemmingsToSave()));

        current.getAvailableSkills().forEach(skillBuilder -> {
            Button btn = new Button(skillBuilder.getDisplayName());
            btn.setOnAction(event -> skillHandler(btn));
            skillBar.getItems().add(0, btn);
        });

        if (current.getMusics().get(0) != null){
            music = new MediaPlayer(current.getMusics().get(0));
            music.setVolume(music.getVolume()/4);
            music.setOnEndOfMedia(new Runnable() {
                @Override
                public void run() {
                    music.seek(Duration.ZERO);
                }
            });
            music.play();
        }

        gameDisplay.onMouseClickedProperty().set(this::canvasClick);

        displayLoop.setOnFinished(e-> {
            this.gameLoop();
            time1 = (new Date()).getTime();
            displayLoop.playFromStart();
        });
        displayLoop.play();

    }

    /**
     * Handle click on a skill button
     * @param btn a Button
     */
    private void skillHandler(Button btn){
        skillBar.getItems().forEach(button -> button.setDisable(false));
        btn.setDisable(true);
        System.out.println(btn.getText());
    }

    /**
     * Handle click on canvas
     * @param e as MouseEvent
     */
    private void canvasClick(MouseEvent e){
        if (!killEveryone) {
            Button skillBtn = (Button) skillBar.getItems().stream().filter(btn -> (btn instanceof Button && btn.isDisabled())).findAny().orElse(null);
            if (skillBtn != null) {
                SkillBuilder builder = current.getAvailableSkills().stream().filter(skillBuilder ->
                        skillBuilder.getDisplayName().equals(skillBtn.getText())
                ).findFirst().get();
                current.setSkillOnLemming(builder,
                        e.getX() + mainPane.getHvalue(),
                        e.getY() + mainPane.getVvalue()
                );
                skillBtn.setText(builder.getDisplayName());
            }
        }
    }

    /**
     * Clean everything (stop music…)
     */
    @Override
    public void clean() {

        current.destroy();
        displayLoop.setOnFinished(e->{});
        displayLoop.stop();
        this.app = null;
        if (music != null) music.stop();

    }


    /**
     * Get a reverence over the main app
     * @param app Crenaud2
     */
    @Override
    public void setApplication(Crenaud2 app) {
        this.app = app;
    }

    /**
     * Set the code to display to reach this level
     *
     * @param code
     */
    @Override
    public void setCode(String code) {
        codeLabel.setText(code);
    }

    /**
     * Draw on canvas
     */
    private void gameLoop() {

        GraphicsContext gc = gameDisplay.getGraphicsContext2D();
        Long time2 = new Long(new Date().getTime());
        double lambda = ((double) time2) - ((double) time1);
        double xOffset = mainPane.getHvalue();
        double yOffset = mainPane.getVvalue();
        mainPane.setHmax(gameDisplay.getWidth() + 10);
        mainPane.setVmax(gameDisplay.getHeight() + 10);


        gc.clearRect(0, 0, gameDisplay.getWidth(), gameDisplay.getHeight());
        if (this.current.getMap().getLayers() != null) {

            this.current.getMap().getLayers().forEach(l -> {

                l.getBlocks().forEach(b -> {

                    if (b != null && b.getImage() != null && b.getPosition() != null) {

                        gc.drawImage(b.getImage(),
                                b.getPosition().getX() - b.getImage().getWidth()/2 - xOffset,
                                b.getPosition().getY() - b.getImage().getWidth()/2 - yOffset
                        );

                    }

                });

            });

        }

        current.getStairs().forEach(stair -> {
            gc.drawImage(stair.getImage(),
                   stair.getPosition().getX() - stair.getImage().getWidth()/2 - xOffset,
                    stair.getPosition().getY() - stair.getImage().getHeight()/2 - yOffset
            );
        });

        this.current.update(lambda/1000);

        this.current.getAliveLemmings().forEach(lemming -> {
            Image img = lemming.getImage();
            gc.drawImage(img,
                    0,
                    0,
                    img.getWidth(),
                    img.getHeight(),
                    lemming.getPosition().getX() + lemming.getHorizontalDirection() * img.getWidth()/2 - xOffset,
                    lemming.getPosition().getY() - img.getHeight()/2 - yOffset,
                    -lemming.getHorizontalDirection() * img.getWidth(),
                    img.getHeight());

            if(lemming.getSkill() instanceof SuicideSkill){
                gc.strokeText(String.valueOf(String.valueOf(((SuicideSkill) lemming.getSkill()).getCountdown()).charAt(0)),
                        lemming.getPosition().getX() - img.getWidth()/4,
                        lemming.getPosition().getY() - img.getHeight()/2);
            }

        });

        if(Crenaud2.isDebug()){
           displayHitboxes(gc);
        }

    }

    private void displayHitboxes(GraphicsContext gc){

        double xOffset = mainPane.getHvalue();
        double yOffset = mainPane.getVvalue();

        this.current.getMap().getLayers().forEach(layer ->{

                layer.getBlocks().forEach(block ->{

                    if(block.hasPhysicalBody){

                        Hitbox tempHitbox = block.getHitbox();
                        gc.strokeRect(block.getPosition().getX() - block.getImage().getWidth()/2 - xOffset,
                                block.getPosition().getY() - block.getImage().getHeight()/2 - yOffset,
                                tempHitbox.getXmax() - tempHitbox.getXmin(),
                                tempHitbox.getYmax() - tempHitbox.getYmin());

                    }

                });

        });

        this.current.getAliveLemmings().forEach(lemming -> {

            Hitbox tempHitbox = lemming.getHitbox();
            gc.strokeRect(tempHitbox.getCenter().getX() - lemming.getImage().getWidth()/2 - xOffset,
                                tempHitbox.getCenter().getY() - lemming.getImage().getHeight()/2 - yOffset,
                                tempHitbox.getXmax() - tempHitbox.getXmin(),
                                tempHitbox.getYmax() - tempHitbox.getYmin());

            gc.strokeArc(lemming.getPosition().getX() - 3 - xOffset, lemming.getPosition().getY() - 3 - yOffset, 6.0,6.0, 0.0, 360.0, ArcType.ROUND);

        });

        this.current.getStairs().forEach(stair -> {
            Hitbox tempHitbox = stair.getHitbox();
            gc.strokeRect(tempHitbox.getCenter().getX() - stair.getImage().getWidth()/2 - xOffset,
                                tempHitbox.getCenter().getY() - stair.getImage().getHeight()/2 - yOffset,
                                tempHitbox.getXmax() - tempHitbox.getXmin(),
                                tempHitbox.getYmax() - tempHitbox.getYmin());

        });

    }

    @FXML private void handleKillEveryone(){
        killEveryone = true;
        current.killEveryone();
        skillBar.getItems().forEach(button -> button.setDisable(true));
    }
}
