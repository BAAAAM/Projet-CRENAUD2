package crenaud2.physics;

/**
 * Created by aethor on 29/04/17.
 */
public class Vector {

    /*---ATTRIBUTES---*/

    private double x;
    private double y;

    /*---CONSTRUCTORS---*/

    public Vector(){

        this.x = 0.0;
        this.y = 0.0;

    }

    public Vector(double x, double y){

        this.x = x;
        this.y = y;

    }

    public Vector(Vector v){

        this.x = v.getX();
        this.y = v.getY();

    }

    /*---METHODS---*/

    public Vector negate(){
        return new Vector(- this.x, -this.y);
    }

    // Getters

    /**
     * Return the x coordinate of the Vector
     * @return x a Double
     */
    public double getX() {
        return x;
    }

    /**
     * Return the y coordinate of the Vector
     * @return y a Double
     */
    public double getY() {
        return y;
    }

    public Vector getUnitary(){
        return new Vector(this.getX()/this.length(), this.getY()/this.length());
    }
    // Setters

    /**
     * Set the x coordinate of the Vector with a given Double value
     * @param x a Double
     */
    public void setX(double x) {
        this.x = x;
    }

    /**
     * Set the y coordinate of the Vector with a given Double value
     * @param y a Double
     */
    public void setY(double y) {
        this.y = y;
    }

    /**
     * Set the coordinates of the Vector with thow given Double values
     * @param x a Double
     * @param y a Double
     */
    public void setCoordinates(double x, double y){

        this.x = x;
        this.y = y;

    }

    // Adders

    /**
     * Return the result of the addition of the current Vector and a given Vector
     * @param V a Vector
     * @return a Vector
     */
    public Vector add(Vector V){

        Vector Va = new Vector(this.x + V.getX(), this.y + V.getY());
        return Va;

    }

    /**
     * Return the result of the subtraction of the current Vector and a given Vector
     * @param V a Vector
     * @return a Vector
     */
    public Vector substract(Vector V){

        Vector Va = new Vector(this.x - V.getX(), this.y - V.getY());
        return Va;

    }

    public double distance(Vector V){
        return this.substract(V).length();
    }

    // ToString

    /**
     * Return the String object that contains the coordinates of the current Vector
     * @return a String
     */
    public String toString(){

        return "( " + this.x + " , " + this.y + " )";

    }

    // Testers

    /**
     * Return true if the current Vector coordinates equal the coordinates of a Vector, false otherwise
     * @param vector a Vector
     * @return a Boolean
     */
    public boolean equals(Vector vector){

        if(this.x == vector.getX() && this.y == vector.getY()){
            return true;
        }
        else{
            return false;
        }

    }

    /**
     * Return true if the current Vector is perpendicular to a given Vector, false otherwise
     * @param V a Vector
     * @return a Boolean
     */
    public boolean isPerpendicular(Vector V){

        boolean answ = false;
        if ( this.dot(V) != 0){
            answ = true;
        }
        return answ;

    }

    // Operations

    /**
     * Return the length of the current Vector
     * @return a Double
     */
    public double length(){

        return Math.sqrt(this.x * this.x + this.y * this.y);

    }

    /**
     * Return the dot product of the current Vector and a given Vector
     * @param V a Vector
     * @return a Double
     */
    public double dot(Vector V){

        return (this.x * V.getX() + this.y * V.getY());

    }


    /**
     * Return the result of the Scalar product of the current Vector and a given Double
     * @param scal a Double
     * @return a Vector
     */
    public Vector mulScalar(double scal){

        return new Vector(this.getX()*scal, this.getY()*scal);

    }








}
