package crenaud2.physics;

/**
 * Created by aethor on 28/04/17.
 */
/*HELLO im vincent
no shit sherlock
 */
public class Hitbox {

    private double xmin;
    private double ymin;
    private double xmax;
    private double ymax;

    /*---CONSTRUCTORS---*/

    public Hitbox(){

        this.xmin = 0;
        this.ymin = 0;
        this.xmax = 0;
        this.ymax = 0;

    }

    public Hitbox(double xmin, double ymin, double xmax, double ymax){

        if(xmin<=xmax && ymin<=ymax){
            this.xmin = xmin;
            this.ymin = ymin;
            this.xmax = xmax;
            this.ymax = ymax;
        }else{
            this.xmin = 0;
            this.ymin = 0;
            this.xmax = 0;
            this.ymax = 0;
        }


    }

    public Hitbox(Vector origin,double xmin, double ymin, double xmax, double ymax){

        if(xmin<=xmax && ymin<=ymax){
            this.xmin = xmin;
            this.ymin = ymin;
            this.xmax = xmax;
            this.ymax = ymax;
        }else{
            this.xmin = 0;
            this.ymin = 0;
            this.xmax = 0;
            this.ymax = 0;
        }

    }

    /*---METHODS---*/

    // Getters

    public double getXmax() {
        return xmax;
    }

    public double getYmax() {
        return ymax;
    }

    public double getXmin() {
        return xmin;
    }

    public double getYmin() {
        return ymin;
    }

    // Setters

    public boolean setXmin(double xmin) {

        if(xmin<=this.xmax){
            this.xmin = xmin;
            return true;
        }else{
            return false;
        }

    }

    public boolean setYmin(double ymin) {

        if(ymin<=this.ymax){
            this.ymin = ymin;
            return true;
        }else{
            return false;
        }

    }

    public boolean setXmax(double xmax) {

        if(xmax>=this.xmin){
            this.xmax = xmax;
            return true;
        }else{
            return false;
        }

    }

    public boolean setYmax(double ymax) {

        if(ymax>=this.ymin){
            this.ymax = ymax;
            return true;
        }else{
            return false;
        }

    }

    public Vector getCenter(){

        return new Vector(xmin + (xmax - xmin)/2,ymin + (ymax - ymin)/2);

    }

    public void move(Vector position){
        Vector oldPosition = new Vector((xmax - xmin)/2, (ymax - ymin)/2);

        xmax = position.getX() + oldPosition.getX();
        xmin = position.getX() - oldPosition.getX();
        ymax = position.getY() + oldPosition.getY();
        ymin = position.getY() - oldPosition.getY();

    }

    public void move(double x, double y){
        Vector oldPosition = new Vector((xmax - xmin)/2, (ymax - ymin)/2);

        xmax = x + oldPosition.getX();
        xmin = x - oldPosition.getX();
        ymax = y + oldPosition.getY();
        ymin = y - oldPosition.getY();

    }

    // Testers

    /**
     * Check if two hitboxes are colliding
     * @param other another hitbox
     * @return true if both hitboxes collide
     */
    public boolean isColliding(Hitbox other){

        //return (( (xmin >= other.getXmin() && xmin <= other.getXmax()) || (xmax >= other.getXmin() && xmax <= other.getXmax())) && ( (ymin >= other.getYmin() && ymin <= other.getYmax()) || (ymax >= other.getYmin() && ymax <= other.getYmax())));
        return !((other.getXmin() > xmax) || (other.getYmin() > ymax) || (other.getXmax() < xmin) || (other.getYmax() < ymin));

    }

    //TODO
    // Faire une fonction qui retourne la normale à une collision

    // Faire une fonction qui retourn l'angle résultant d'une collision

    public boolean equals(Hitbox hitbox){

        if(xmin == hitbox.getXmin() && ymin == hitbox.getYmin() && xmax == hitbox.getXmax() && ymax == hitbox.getYmax()){
            return true;
        }
        else{
            return false;
        }

    }

    public String toString(){

        return "Hitbox\n\txmin : " + xmin + "\txmax : " + xmax + "\n\tymin : " + ymin + "\tymax : " + ymax;

    }

    /**
     * Detect if the given point is inside the hitbox
     * @param x double
     * @param y double
     * @return boolean
     */
    public boolean pointIsInHitbox(double x, double y){
        return (x < xmax && x > xmin && y < ymax && y > ymin);
    }


}

