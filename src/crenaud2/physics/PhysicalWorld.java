package crenaud2.physics;

import crenaud2.lemmings.Lemming;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 * Created by aethor on 28/04/17.
 * CORE OF THE SLI PHYSICS ENGINE PROJECT
 */
public class PhysicalWorld {

    private ArrayList<PhysicalObject> objectList;
    private LinkedList<PhysicalObject> delayedAdd = new LinkedList<>();
    private LinkedList<PhysicalObject> delayedRemove = new LinkedList<>();


    public PhysicalWorld(ArrayList<PhysicalObject> objectList) {

        this.objectList = objectList;

    }

    public PhysicalWorld(){

        this.objectList = new ArrayList<PhysicalObject>();

    }

    public void update(double lambda) {

        //updating objects status
        addDelayedObject();
        removeDelayedObject();

        for(PhysicalObject object : this.objectList){

            if ((object instanceof Lemming && !((Lemming) object).getProperties().contains("block")) ||
                    object instanceof MovableObject) {

                boolean collided = false;

                for(PhysicalObject collidingObject : this.objectList){//entering collision detection

                    if (object != collidingObject && object.isColliding(collidingObject)){//detect one collision ( object has to be different of collidingObject or object could collide with himself )

                        object.collide(collidingObject);
                        collided = true;

                    }


                }

                if(!collided){//actions when the object hasn't collided
                        object.notCollided();
                }

                object.update(lambda);

            }

        }

    }

    public String toString(){

        return objectList.toString();

    }

    public boolean equals(PhysicalWorld world){

        return objectList.equals(world.getObjectList());

    }

    public static void main(String argv[]) {

        System.out.println("Hello world");
        int i;
        Vector pos = new Vector(0, 0);
        Vector sp = new Vector(3, 0);

        ArrayList<PhysicalObject> objects = new ArrayList<>();
        PhysicalWorld world = new PhysicalWorld(objects);
        MovableObject test = new MovableObject(pos, sp, 10);
        test.setHitbox(new Hitbox(test.getPosition(), -5,-5,5,5));
        //test.setHitbox(new HitboxCircle(test.getPosition(),3));
        objects.add(test);

        for(i = 0; i <= 2000; i++){
            world.update(1);
        }

    }

    public ArrayList<PhysicalObject> getObjectList() {
        return objectList;
    }

    public void setObjectList(ArrayList<PhysicalObject> objectList) {
        this.objectList = objectList;
    }

    /**
     * Add objects to physical world when the update function is finished
     * Prevent from java's iterator faggotry
     * @param obj a PhysicalObject
     */
    public void delayedAddToObjectList(PhysicalObject obj){
        delayedAdd.add(obj);
    }

    private void addDelayedObject(){
        objectList.addAll(delayedAdd);
        delayedAdd.clear();
    }

    public void delayedRemoveFromObjectList(PhysicalObject obj) {
        delayedRemove.add(obj);
    }

    private void removeDelayedObject(){
        objectList.removeAll(delayedRemove);
        delayedRemove.clear();
    }
}
