package crenaud2.physics;

import java.awt.geom.Point2D;
import java.util.Map;

/**
 * Created by aethor on 28/04/17.
 */
public class PhysicalObject {

    // Ajuster les hitbox !!!!!
    protected Hitbox hitbox;
    protected Vector position;
    protected double bounce; //TODO


    public PhysicalObject(Vector position){

        this.position = position;
        this.hitbox = new Hitbox();

    }

    public PhysicalObject(double x, double y){

        this.position.setX(x);
        this.position.setY(y);

    }

    public PhysicalObject(Vector position, Hitbox hitbox){
        this(position);
        this.hitbox = hitbox;
    }

    public PhysicalObject(double x, double y, Hitbox hitbox){
        this(x, y);
        this.hitbox = hitbox;
    }

    public PhysicalObject(){

        this.position = new Vector(0, 0);
        this.hitbox = new Hitbox();

    }

    public boolean isColliding(PhysicalObject collidingObject){
        return this.hitbox.isColliding(collidingObject.getHitbox());
    }

    public void collide(PhysicalObject collidingObject, double direction){
        //NUTHIN
    }


    public double getCollisionDirection(PhysicalObject collidingObject){

        Vector centerToCenter = new Vector(collidingObject.getPosition().getX() - getPosition().getX(), collidingObject.getPosition().getY() - getPosition().getY());
        Vector base = new Vector(this.hitbox.getXmax() - this.getPosition().getX(),0);
        double theta = Math.acos((centerToCenter.dot(base))/(centerToCenter.length() * base.length()));
        return theta;

    }

    public String toString(){

        return "coordinates : " + position.toString() + "\n";

    }

    public boolean equals(PhysicalObject object){//position has no importance : it can be the same object even if they aren't in the same position

        if(hitbox.equals(object.getHitbox())){
            return true;
        }
        else{
            return false;
        }
    }

    public Hitbox getHitbox() {
        return hitbox;
    }

    public Hitbox getRect(){ return this.hitbox; }

    public Hitbox getRekt(){ return this.hitbox; }

    public void setHitbox(Hitbox hitbox) {
        this.hitbox = hitbox;
    }

    public Vector getPosition() {
        return position;
    }

    public void setPosition(Vector position) {
        this.position = position;
    }

    public void setPosition(double x, double y){//Todo : it has to set the position of the hitbox too - dont make it external
        this.position.setX(x);
        this.position.setY(y);
    }

    public Vector getSpeed(){
        return new Vector(0, 0);
    }

    public Vector getAcceleration(){
        return new Vector(0, 0);
    }

    public void update(double lambda) {}

    public void collide(PhysicalObject collidingObject) {
    }

    public void notCollided() {
    }
}

