package crenaud2.physics;
import java.util.ArrayList;
import java.util.HashMap;

import static java.lang.Math.sqrt;

/**
 * Created by aethor on 29/04/17.
 */

/*Please be careful when directly setting acceleration, you might break something
actually nvm, your change would be REKT next frame
 */
public class MovableObject extends PhysicalObject{

    /*---ATTRIBUTES---*/

    protected double mass;
    protected Vector speed;
    protected Vector acceleration;
    protected HashMap<String, Vector> forces;

    /*---CONSTRUCTORS---*/

    public MovableObject(){

        this.speed = new Vector();
        this.acceleration = new Vector();
        this.mass = 0;

    }

    public MovableObject(Vector position, Vector speed, double mass){ //standard constructor, should be used most of the time

        this.position = position;
        this.speed = speed;
        this.mass = mass;

        this.forces = new HashMap<String, Vector>();
        Vector gravity = new Vector(0, 9.809 * mass);
        forces.put("gravity", gravity);

        this.acceleration = this.getSumForces();

    }

    public MovableObject(double x, double y, double mass){

        this(new Vector(x, y), new Vector(0,0), mass);

    }

    public MovableObject(Vector position, double mass){
        this(position.getX(), position.getY(), mass);
    }

    /*---METHODS---*/

    /**
     * update the position, speed and acceleration vectors, and forces applying on the object
     * @param lambda the time between this update and the last one ( 1 / framerate )
     */
    public void update(double lambda){

        Vector normalizedSpeed = new Vector();
        Vector normalizedAcceleration = new Vector();
        Vector sumOfForces = this.getSumForces();

        normalizedAcceleration.setCoordinates(sumOfForces.getX() * lambda, sumOfForces.getY() * lambda);
        this.setSpeed(this.speed.getX() + normalizedAcceleration.getX(), this.speed.getY() + normalizedAcceleration.getY());

        normalizedSpeed.setCoordinates(this.speed.getX() * lambda, this.speed.getY()* lambda);
        this.setPosition(this.position.getX() + normalizedSpeed.getX(), this.position.getY() + normalizedSpeed.getY());

        this.hitbox.move(this.position);

    }

    /**
     * update forces applying on the object
     */
    @Deprecated
    public void updateForces(){

    }

    /**
     * compute and return a Vector representing the sum of forces applying on the object
     * @return A vector representing the sum of forces applying on the object
     */
    public Vector getSumForces(){

        Vector k = new Vector(0, 0); //KDE Vector
        forces.forEach((key, value) -> k.setCoordinates( k.getX() + value.getX()/mass,value.getY()/mass + k.getY()));
        return k;

    }


    public void addUniqForce(String name, Vector force){

        if(!this.forces.containsKey(name)){
            this.forces.put(name, force);
        }

    }

    /**
     * check if two objects are colliding, via their hitboxes
     * @param collidingObject the second object
     * @return return true if both objects are colliding, false otherwise
     */
    public boolean isColliding(PhysicalObject collidingObject){

        return this.hitbox.isColliding(collidingObject.getHitbox());

    }

    /**
     * Apply actions when object is colliding
     * @param collidingObject the object colliding with the present object
     */
    @Override
    public void collide(PhysicalObject collidingObject){

        this.speed.setCoordinates(- this.speed.getX(), - this.speed.getY());

    }

    public void notCollided(){

    }

    public double getSpeedNorm(){

        return sqrt(speed.getX() * speed.getX() + speed.getY() + speed.getY());

    }

    public String toString(){

        return super.toString() + "speed : " + speed.toString() + "\nspeed norm : " + getSpeedNorm();

    }

    public boolean equals(MovableObject object){//position, speed, acceleration and forces have no effect on this
        if(super.equals((PhysicalObject) object) && this.mass == object.getMass()){
            return true;
        }
        else{
            return false;
        }
    }

    public double getMass() {
        return mass;
    }

    public void setMass(double mass) {
        this.mass = mass;
    }

    public Vector getSpeed() {
        return speed;
    }

    public void setSpeed(double x, double y){
        this.speed.setX(x);
        this.speed.setY(y);
    }

    public void setSpeed(Vector speed) {
        this.speed = speed;
    }

    public Vector getAcceleration() {
        return acceleration;
    }

    public void setAcceleration(Vector acceleration) {
        this.acceleration = acceleration;
    }

    public HashMap<String, Vector> getForces() {
        return forces;
    }
}
