package crenaud2.gamecontroller;

import java.io.FileNotFoundException;
import java.util.*;

/**
 * Created by klmp200 on 03/05/2017.
 */

/**
 * Generate a GameController from a simple controller file
 */
public class GameController {

    private int currentStage;
    private List<Stage> stage;
    private Double score;

    /**
     * Generate a new GameController according to a given filepath
     * @param controllerPath as String
     * @throws FileNotFoundException if the file doesn't exist
     */
    public GameController(String controllerPath) throws FileNotFoundException {
        Scanner controller = new Scanner(getClass().getResourceAsStream(controllerPath));
        stage = new ArrayList<>();
        score = 0.0;
        currentStage = 0;

        while (controller.hasNextLine()) stage.add(new Stage(controller.nextLine().split(":")));
    }

    /**
     * Get score of the game
     * @return Double
     */
    public Double getScore() {
        return score;
    }

    /**
     * Add number to score
     * @param score as double
     */
    public void addScore(double score) {
        this.score += score;
    }

    /**
     * Check if another map is available
     * @return boolean
     */
    public boolean hasNextStage(){
        return currentStage < this.stage.size();
    }

    /**
     * Get next map if exists, else get null
     * @return Stage
     */
    public Stage getNextStage(){
        Stage nextLevel = null;
        if (hasNextStage()){
            nextLevel = this.stage.get(this.currentStage);
            this.currentStage++;
        }
        return nextLevel;
    }

    /**
     * Chose a stage according to a given save code
     * @param code a String
     * @return Stage
     */
    public Stage getStageByCode(String code){
        Stage nextLevel = null;
        int i = 0;
        for (Stage s: stage){
            ++i;
            if (s.getCode().equals(code)){
                nextLevel = s;
                this.currentStage = i;
                break;
            }
        }
        return nextLevel;
    }

    /**
     * Get current stage or null if doesn't exist
     * @return Stage
     */
    public Stage getCurrentStage(){
        return this.stage.get(this.currentStage);
    }

    /**
     * Skip a map
     */
    public void skipStage(){
        this.currentStage++;
    }


    /**
     * Start all stages from zero
     */
    public void resetStages(){
        this.currentStage = 0;
    }

    /**
     * Completely reset the game controller
     */
    public void resetGame(){
        this.score = 0.0;
        this.resetStages();
    }

    public static void main(String [] argv){
        try {
            GameController controller = new GameController("/json/controller.godwin");
            controller.stage.forEach(System.out::println);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
