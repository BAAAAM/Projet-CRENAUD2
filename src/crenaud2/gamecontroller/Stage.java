package crenaud2.gamecontroller;

/**
 * Created by klmp200 on 03/05/2017.
 */

/**
 * A step of the game (either a level or a cinematic)
 */
public class Stage {
    String type;
    String filepath;
    String code;

    /**
     * Generate a new map
     * @param type as String
     * @param filepath as String
     */
    public Stage(String type, String filepath, String code) {
        this.type = type;
        this.filepath = filepath;
        this.code = code;
    }

    /**
     * Generate a new map
     * @param array String[] of 2 values
     */
    public Stage(String[] array){
        this(array[0], array[1], array[2]);
    }

    /**
     * Get stage type
     * @return String
     */
    public String getType() {
        return type;
    }

    /**
     * Get stage path
     * @return String
     */
    public String getFilepath() {
        return filepath;
    }

    /**
     * Get the code of the level
     * @return String
     */
    public String getCode() {
        return code;
    }

    @Override
    public String toString() {
        return "Stage{" +
                "type='" + type + '\'' +
                ", filepath='" + filepath + '\'' +
                ", code='" + code + '\'' +
                '}';
    }
}
