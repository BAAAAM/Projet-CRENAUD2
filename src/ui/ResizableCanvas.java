package ui;

import javafx.scene.canvas.Canvas;

/**
 * Created by klmp200 on 09/05/2017.
 */
public class ResizableCanvas extends Canvas {
    @Override
    public boolean isResizable() {
        return true;
    }

    @Override
    public double prefWidth(double height) {
        return getWidth();
    }


    @Override
    public double prefHeight(double width) {
        return getHeight();
    }
}
